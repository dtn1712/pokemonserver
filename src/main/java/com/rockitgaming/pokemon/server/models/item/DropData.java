package com.rockitgaming.pokemon.server.models.item;

import org.simpleframework.xml.Element;

/**
 * Stores drop data for a Pokemon
 */
public class DropData {

    @Element
    private int item;
    @Element
    private int probability;

    public DropData() {}

    public DropData(int item, int probability) {
        this.item = item;
        this.probability = probability;
    }

    public int getItemNumber() {
        return item;
    }

    public int getProbability() {
        return probability;
    }

    public void setItemNumber(int item) {
        this.item = item;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }
}
