package com.rockitgaming.pokemon.server.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.rockitgaming.pokemon.server.ServerConfig;

import java.io.IOException;

public class CacheService {

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> redisConnection;
    private RedisCommands<String, String> redisCommands;

    public static CacheService instance;

    private CacheService() {
        redisClient = RedisClient.create(ServerConfig.redisCacheConnection);
        redisConnection = redisClient.connect();
        redisCommands = redisConnection.sync();
    }

    public static CacheService getInstance() {
        if (instance == null) {
            instance = new CacheService();
        }
        return instance;
    }

    public RedisCommands<String, String> getRedisCommands() {
        return redisCommands;
    }

    public void set(String key, String value) {
        redisCommands.set(key,value);
    }

    public void set(String key, Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String objectData = mapper.writeValueAsString(value);
        redisCommands.set(key,objectData);
    }

    public String get(String key) {
        return redisCommands.get(key);
    }

    public Object get(String key, Class clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String redisData = redisCommands.get(key);
        return mapper.readValue(redisData,clazz);
    }

    public Object getOrSet(String key, Object value, Class clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            String objectData = mapper.writeValueAsString(value);
            redisCommands.set(key,objectData);
            return value;
        } else {
            return mapper.readValue(redisData,clazz);
        }
    }

    public String getOrSet(String key, String value) {
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            redisCommands.set(key,value);
            return value;
        } else {
            return redisData;
        }
    }

    public void delete(String key) {
        redisCommands.del(key);
    }

    public void clearAll() {
        redisCommands.flushall();
    }

    public void reset() {
        redisCommands.reset();
    }

    public void increment(String key, long amount) {
        redisCommands.incrby(key,amount);
    }

    public void decrement(String key, long amount) {
        redisCommands.decrby(key,amount);
    }
}
