package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.constants.ShopInteraction;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class ShopEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player player = session.getPlayer();
        int action = request.readInt();
        int quantity = 1;
        int item;
        switch (action) {
            case ShopInteraction.BUY_ITEM:
                item = request.readInt();
                player.buyItem(item, quantity);
                break;
            case ShopInteraction.SELL_ITEM:
                item = request.readInt();
                player.sellItem(item, quantity);
                break;
            case ShopInteraction.DONE_SHOPPING:
                player.setShopping(false);
                break;
            case ShopInteraction.BUY_MULTIPLE_ITEM:
                item = request.readInt();
                quantity = request.readInt();
                player.buyItem(item, quantity);
                break;
            default:
                player.setShopping(false);
                break;
        }
    }
}
