package com.rockitgaming.pokemon.server.services;

import com.rockitgaming.pokemon.server.Constants;
import com.rockitgaming.pokemon.server.manager.MovementManager;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.models.map.ServerMap;
import com.rockitgaming.pokemon.server.models.map.ServerMapMatrix;
import com.rockitgaming.pokemon.server.feature.battle.impl.NpcSleepTimer;
import com.rockitgaming.pokemon.server.manager.pools.ThreadPoolManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import tiled.io.xml.XMLMapTransformer;

import java.io.File;
import java.util.HashMap;

/**
 * Stores the map matrix and movement managers.
 *
 * @author shadowkanji
 */
public class MovementService {

    private static final Log log = LogFactory.getLog(MovementService.class);

    private final ServerMapMatrix mapMatrix;
    private final MovementManager[] movementManager;
    private final NpcSleepTimer sleepTimer;
    private ThreadPoolManager threadPoolManager = ThreadPoolManager.getInstance();

    /**
     * Default constructor
     */
    public MovementService() {
        movementManager = new MovementManager[Constants.MOVEMENT_THREADS];
        mapMatrix = new ServerMapMatrix();
        sleepTimer = new NpcSleepTimer();
    }

    /**
     * Returns the map matrix
     *
     * @return
     */
    public ServerMapMatrix getMapMatrix() {
        return mapMatrix;
    }

    /**
     * Returns the movement manager with the smallest processing load
     *
     * @return
     */
    public MovementManager getMovementManager() {
        int smallest = 0;
        if (movementManager.length > 1) {
            for (int i = 0; i < movementManager.length; i++) {
                if (movementManager[i].getProcessingLoad() < movementManager[smallest].getProcessingLoad()) {
                    smallest = i;
                }
            }
        }
        if (movementManager[smallest] == null) {
            movementManager[smallest] = new MovementManager();
        }
        if (!movementManager[smallest].isRunning()) {
//            movementManager[smallest].start();
            threadPoolManager.execute(movementManager[smallest]);
        }
        return movementManager[smallest];
    }

    /**
     * Reloads all maps while the server is running. Puts all players in
     * tempMap. An NPC is there to allow them to return to where they last
     * where when they are ready. Optionally, we can skip saving players in a
     * temporary map.
     *
     * @param forceSkip
     */
    public void reloadMaps(boolean forceSkip) {
        /* First move all players out of their maps */
        if (!forceSkip) {
            HashMap<String, Player> players;
            for (int x = 0; x < 100; x++)
                for (int y = 0; y < 100; y++)
                    if (mapMatrix.getMapByRealPosition(x, y) != null) {
                        players = mapMatrix.getMapByRealPosition(x, y).getPlayers();
                        for (Player player : players.values()) {
                            player.setLastHeal(player.getX(), player.getY(), player.getMapX(), player.getMapY());
                            player.setMap(mapMatrix.getMapByRealPosition(x, y), player.getFacing());
                        }
                    }
        }
		/* Reload all the maps */
        XMLMapTransformer xmlLoader = new XMLMapTransformer();
        for (int x = -50; x < 50; x++) {
            for (int y = -50; y < 50; y++) {
                loadFileMapData(xmlLoader, x,y);
            }
        }
        log.info("Maps loaded");
    }

    private void loadFileMapData(XMLMapTransformer xmlLoader, int x, int y) {
        File nextMap = new File("res/maps/" + String.valueOf(x) + "." + String.valueOf(y) + ".tmx");
        if (nextMap.exists()) {
            try {
                ServerMap map = new ServerMap(xmlLoader.readMap(nextMap.getCanonicalPath()), x, y);
                map.setMapMatrix(mapMatrix);
                map.loadData();
                mapMatrix.setMap(map, x + 50, y + 50);
                log.info("Loaded map: " + x + ", " + y);
            } catch (Exception e) {
                log.error("Error loading " + x + "." + y + ".tmx - Bad map file");
                mapMatrix.setMap(null, x + 50, y + 50);
            }
        }
    }

    /**
     * Removes a player from the movement service
     *
     * @param username
     */
    public void removePlayer(String username) {
        for (int i = 0; i < movementManager.length; i++) {
            if (movementManager[i].removePlayer(username)) {
                break;
            }
        }
    }

    /**
     * Starts the movement service
     */
    public void start() {
        this.reloadMaps(true);
        threadPoolManager.execute(sleepTimer);
        for (int i = 0; i < movementManager.length; i++) {
            movementManager[i] = new MovementManager();
            threadPoolManager.execute(movementManager[i]);
        }
        log.info("Movement Service started");
    }

    /**
     * Stops the movement service
     */
    public void stop() {
        sleepTimer.stop();
        for (int i = 0; i < movementManager.length; i++)
            movementManager[i].stop();
        log.info("Movement Service stopped");
    }
}