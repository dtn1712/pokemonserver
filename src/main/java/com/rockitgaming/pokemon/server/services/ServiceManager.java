package com.rockitgaming.pokemon.server.services;

import com.rockitgaming.pokemon.server.models.SpriteList;
import com.rockitgaming.pokemon.server.models.item.ItemDatabase;
import com.rockitgaming.pokemon.server.ServerException;
import com.rockitgaming.pokemon.server.manager.pools.ThreadPoolManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Handles all services on the game server
 */
public class ServiceManager {

    private static final Log log = LogFactory.getLog(ServiceManager.class);

    private DataService dataService;
    private ItemDatabase itemDatabase;
    private MovementService movementService;
    private GameRunnerService gameRunnerService;
    private SpriteList sprites;
    private TimeService timeService;

    private ThreadPoolManager threadPoolManager = ThreadPoolManager.getInstance();

    public ServiceManager() {
        timeService = new TimeService();
        dataService = new DataService();
        gameRunnerService = new GameRunnerService();
        itemDatabase = new ItemDatabase();
        movementService = new MovementService();
        sprites = new SpriteList();
    }

    public DataService getDataService() {
        return dataService;
    }

    public ItemDatabase getItemDatabase() {
        return itemDatabase;
    }

    public MovementService getMovementService() {
        return movementService;
    }

    public GameRunnerService getGameRunnerService() {
        return gameRunnerService;
    }

    public SpriteList getSpriteList() {
        return sprites;
    }


    public TimeService getTimeService() {
        return timeService;
    }

    public void start() throws ServerException {
        sprites.initialize();
        itemDatabase.initialize();

        movementService.start();
        gameRunnerService.start();

        threadPoolManager.execute(timeService);

        log.info("Service Manager startup completed.");
    }

    public void stop() {
        timeService.stop();
        movementService.stop();
        gameRunnerService.stop();

        log.info("Service Manager stopped.");
    }


}
