package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class DropItemEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player p = session.getPlayer();
        int item = request.readInt();
        if (p.getBag().removeItem(item, 1)) {
            message.init(ClientPacket.REMOVE_ITEM_BAG.getValue());
            message.addInt(item);
            message.addInt(1);
            session.Send(message);
        }
    }
}
