package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.models.entity.Player.RequestType;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class TradeRequestEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player p = session.getPlayer();
        String player = request.readString();
        if (ActiveConnections.getPlayer(player) != null) {
            ServerMessage bRequest = new ServerMessage(ClientPacket.TRADE_REQUEST);
            bRequest.addString(p.getName());
            ActiveConnections.getPlayer(player).getSession().Send(bRequest);
            p.addRequest(player, RequestType.TRADE);
        }
    }

}
