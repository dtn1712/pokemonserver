package com.rockitgaming.pokemon.server.services;

import com.rockitgaming.pokemon.server.ServerConfig;
import com.rockitgaming.pokemon.server.ServerException;
import com.rockitgaming.pokemon.server.manager.DatabaseManager;
import com.rockitgaming.pokemon.server.manager.SaveManager;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.SocketConnection;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.manager.LoginManager;
import com.rockitgaming.pokemon.server.manager.LogoutManager;
import com.rockitgaming.pokemon.server.manager.RegistrationManager;
import com.rockitgaming.pokemon.server.manager.ChatManager;
import com.rockitgaming.pokemon.server.manager.pools.ThreadPoolManager;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Handles all networking
 *
 * @author shadowkanji
 */
public class GameRunnerService {

    private static final Log log = LogFactory.getLog(GameRunnerService.class);

    private ThreadPoolManager threadPoolManager = ThreadPoolManager.getInstance();

    private static SocketConnection socketConnection;
    private final ChatManager[] chatManagers;
    private final LoginManager loginManager;
    private final LogoutManager logoutManager;
    private final RegistrationManager registrationManager;
    private final SaveManager saveManager;
    private DatabaseManager database;
    private Thread autoSaver;
    protected boolean shouldSave = true;

    /**
     * Default constructor
     */
    public GameRunnerService() {
        database = DatabaseManager.getInstance();
        saveManager = new SaveManager();
        logoutManager = new LogoutManager(saveManager);
        loginManager = new LoginManager();
        registrationManager = new RegistrationManager();
        chatManagers = new ChatManager[3];
        autoSaver = new Thread(new SaveThread());
        threadPoolManager.execute(autoSaver);
    }

    public ChatManager getChatManager() {
        int smallest = 0;
        for (int i = 1; i < chatManagers.length; i++)
            if (chatManagers[i].getProcessingLoad() < chatManagers[smallest].getProcessingLoad())
                smallest = i;
        return chatManagers[smallest];
    }

    public SocketConnection getConnections() {
        return socketConnection;
    }

    public LoginManager getLoginManager() {
        return loginManager;
    }

    public LogoutManager getLogoutManager() {
        return logoutManager;
    }

    public RegistrationManager getRegistrationManager() {
        return registrationManager;
    }

    public SaveManager getSaveManager() {
        return saveManager;
    }

    public void logoutAll() {
        loginManager.stop();
        autoSaver.interrupt();

        /* Queue all players to be saved */
        for (Session session : ActiveConnections.allSessions().values()) {
            if (session.getPlayer() != null) {
                logoutManager.queuePlayer(session.getPlayer());
            }
        }

        /* Since the method is called during a server shutdown, wait for all players to be logged out */
        while (logoutManager.getPlayerAmount() > 0);

        logoutManager.stop();
    }

    public void start() throws ServerException {

		/* Ensure anyone still marked as logged in on this server is unmarked */
        database.query("UPDATE `pn_members` SET `lastLoginServer` = 'null' WHERE `lastLoginServer` = '" + ServerConfig.serverIp + "';");

		/* Start the login/logout managers. */
        threadPoolManager.execute(logoutManager);
        threadPoolManager.execute(loginManager);
        socketConnection = new SocketConnection(logoutManager);
        socketConnection.start();
        log.info("Server started on port " + ServerConfig.serverPort);
        for (int i = 0; i < chatManagers.length; i++) {
            chatManagers[i] = new ChatManager();
            chatManagers[i].start();
        }
        log.info("Network Service started.");

    }

    public void stop() {
        socketConnection.stop();
        logoutAll();
        log.info("Logged out all players.");
        for (ChatManager chatManager : chatManagers)
            chatManager.stop();
        log.info("Network Service stopped.");
    }

    private class SaveThread implements Runnable {

        private int saveInterval = 1000 * 60 * 10;

        @Override
        public void run() {
            while (shouldSave) {
                if (ActiveConnections.getActiveConnections() > 0) {
                    System.out.println("Saving all players");
					/* Queue all players to be saved */
                    for (Session session : ActiveConnections.allSessions().values()) {
                        if (session.getPlayer() != null) {
                            ServerMessage message = new ServerMessage(ClientPacket.SERVER_ANNOUNCEMENT);
                            message.addString("Saving...");
                            session.Send(message);
                            if (saveManager.savePlayer(session.getPlayer()) == 0) {
                                ServerMessage succesmg = new ServerMessage(ClientPacket.SERVER_ANNOUNCEMENT);
                                succesmg.addString("Save succesfull.");
                                session.Send(succesmg);
                            } else {
                                ServerMessage failmsg = new ServerMessage(ClientPacket.SERVER_ANNOUNCEMENT);
                                failmsg.addString("Save Failed.");
                                session.Send(failmsg);
                                System.err.println("Error saving player" + session.getPlayer().getName() + " " + session.getPlayer().getId());
                            }
                        } else {
							/* Attempted save before the client logged in, or session is not a Player. */
                        }
                    }
                }
                try {
                    Thread.sleep(saveInterval);
                } catch (InterruptedException e) {
                    shouldSave = false;
                    log.error("autoSaver has been stopped!",e);
                }
            }
        }
    }
}
