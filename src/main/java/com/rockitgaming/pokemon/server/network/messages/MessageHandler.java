package com.rockitgaming.pokemon.server.network.messages;

import com.rockitgaming.pokemon.server.network.messages.events.AcceptRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.AddFriendEvent;
import com.rockitgaming.pokemon.server.network.messages.events.AllowEvolutionEvent;
import com.rockitgaming.pokemon.server.network.messages.events.BattleRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.BoxInfoEvent;
import com.rockitgaming.pokemon.server.network.messages.events.CanLearnMoveEvent;
import com.rockitgaming.pokemon.server.network.messages.events.CancelEvolutionEvent;
import com.rockitgaming.pokemon.server.network.messages.events.CancelTradeEvent;
import com.rockitgaming.pokemon.server.network.messages.events.ChangePasswordEvent;
import com.rockitgaming.pokemon.server.network.messages.events.ChatEvent;
import com.rockitgaming.pokemon.server.network.messages.events.CreatePokemonOutPokeball;
import com.rockitgaming.pokemon.server.network.messages.events.DeclineRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.DropItemEvent;
import com.rockitgaming.pokemon.server.network.messages.events.FinishBoxingEvent;
import com.rockitgaming.pokemon.server.network.messages.events.FinishTalkingEvent;
import com.rockitgaming.pokemon.server.network.messages.events.ForceLoginEvent;
import com.rockitgaming.pokemon.server.network.messages.events.GiveItemEvent;
import com.rockitgaming.pokemon.server.network.messages.events.LoginEvent;
import com.rockitgaming.pokemon.server.network.messages.events.LogoutRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.MoveDownEvent;
import com.rockitgaming.pokemon.server.network.messages.events.MoveLearnRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.MoveLeftEvent;
import com.rockitgaming.pokemon.server.network.messages.events.MoveRightEvent;
import com.rockitgaming.pokemon.server.network.messages.events.MoveUpEvent;
import com.rockitgaming.pokemon.server.network.messages.events.PartySwapEvent;
import com.rockitgaming.pokemon.server.network.messages.events.PlayerCommandEvent;
import com.rockitgaming.pokemon.server.network.messages.events.PlayerFlyCommand;
import com.rockitgaming.pokemon.server.network.messages.events.PlayerLastPosition;
import com.rockitgaming.pokemon.server.network.messages.events.PlayerResynchronize;
import com.rockitgaming.pokemon.server.network.messages.events.PokemonInfoRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.PokemonSwitchEvent;
import com.rockitgaming.pokemon.server.network.messages.events.RegisterEvent;
import com.rockitgaming.pokemon.server.network.messages.events.ReleasePokemonEvent;
import com.rockitgaming.pokemon.server.network.messages.events.RemoveFriendEvent;
import com.rockitgaming.pokemon.server.network.messages.events.RunEvent;
import com.rockitgaming.pokemon.server.network.messages.events.SelectedMoveEvent;
import com.rockitgaming.pokemon.server.network.messages.events.ShopEvent;
import com.rockitgaming.pokemon.server.network.messages.events.SpriteEvent;
import com.rockitgaming.pokemon.server.network.messages.events.StartBattlefrontierEvent;
import com.rockitgaming.pokemon.server.network.messages.events.StartTalkingEvent;
import com.rockitgaming.pokemon.server.network.messages.events.SwapPokemonBoxPartyEvent;
import com.rockitgaming.pokemon.server.network.messages.events.TradeCancelOfferEvent;
import com.rockitgaming.pokemon.server.network.messages.events.TradeOfferEvent;
import com.rockitgaming.pokemon.server.network.messages.events.TradeReadyEvent;
import com.rockitgaming.pokemon.server.network.messages.events.TradeRequestEvent;
import com.rockitgaming.pokemon.server.network.messages.events.TravelEvent;
import com.rockitgaming.pokemon.server.network.messages.events.UnableLearnMoveEvent;
import com.rockitgaming.pokemon.server.network.messages.events.UseItemEvent;

import java.util.HashMap;
import java.util.Map;

public class MessageHandler // xu ly message 
{
    private Map<Integer, MessageEvent> messages;

    public MessageHandler() {
        messages = new HashMap<>();
    }

    public boolean contains(int id) {
        return messages.containsKey(id);
    }

    public MessageEvent get(int id) {
        if (messages.containsKey(id))
            return messages.get(id);
        else
            return null;
    }

    public void register() {
        messages.put(1, new RegisterEvent());
        messages.put(2, new ChangePasswordEvent());
        messages.put(3, new ForceLoginEvent());
        messages.put(4, new MoveUpEvent());
        messages.put(5, new MoveDownEvent());
        messages.put(6, new MoveLeftEvent());
        messages.put(7, new MoveRightEvent());
        messages.put(8, new CanLearnMoveEvent());
        messages.put(9, new UnableLearnMoveEvent());
        messages.put(10, new CancelEvolutionEvent());
        messages.put(11, new AllowEvolutionEvent());
        messages.put(12, new PartySwapEvent());
        messages.put(13, new ShopEvent());
        messages.put(14, new SpriteEvent());
        messages.put(15, new BattleRequestEvent());
        messages.put(16, new TradeRequestEvent());
        messages.put(17, new AcceptRequestEvent());
        messages.put(18, new DeclineRequestEvent());
        messages.put(19, new BoxInfoEvent());
        messages.put(20, new ReleasePokemonEvent());
        messages.put(21, new SwapPokemonBoxPartyEvent());
        messages.put(22, new FinishBoxingEvent());
        messages.put(23, new PokemonInfoRequestEvent()); // TODO: request pokemon info (can be all kinds of things. request a summary, info on stats, movesets, etc ...)
        messages.put(24, new MoveLearnRequestEvent()); // only for movetutor/moverelearner
        // messages.put(25, new ());
        // messages.put(26, new ());
        // messages.put(27, new ());
        // messages.put(28, new ());
        // messages.put(29, new ());
        // messages.put(30, new ());
        // messages.put(31, new ());
        // messages.put(32, new ());
        messages.put(33, new PlayerCommandEvent());
        // messages.put(34, new ());
        messages.put(35, new SelectedMoveEvent());
        messages.put(36, new PokemonSwitchEvent());
        messages.put(37, new RunEvent());
        messages.put(38, new AddFriendEvent());
        messages.put(39, new RemoveFriendEvent());
        messages.put(40, new UseItemEvent());
        messages.put(41, new DropItemEvent());
        messages.put(42, new TradeOfferEvent());
        messages.put(43, new TradeReadyEvent());
        messages.put(44, new TradeCancelOfferEvent());
        messages.put(45, new CancelTradeEvent());
        messages.put(46, new ChatEvent());
        messages.put(47, new StartTalkingEvent());
        messages.put(48, new FinishTalkingEvent());
        messages.put(49, new LogoutRequestEvent());
        messages.put(50, new LoginEvent());
        messages.put(51, new GiveItemEvent());
        // messages.put(52, new ());
        messages.put(53, new TravelEvent());
        // messages.put(54, new ());
        // messages.put(55, new ());
        // messages.put(56, new ());
        // messages.put(57, new ());
        messages.put(58, new StartBattlefrontierEvent());
        /* TODO: Use commented numbers for future messages. */
        messages.put(59, new PlayerResynchronize());
        messages.put(60, new CreatePokemonOutPokeball());
        messages.put(61, new PlayerLastPosition());
        messages.put(62, new PlayerFlyCommand());
    }
}
