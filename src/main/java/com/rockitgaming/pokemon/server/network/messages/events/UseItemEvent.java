package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.ItemProcessor;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class UseItemEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player p = session.getPlayer();
        // Use an item, applies inside and outside of battle
        String[] details = request.readString().split(",");
        new Thread(new ItemProcessor(p, details), "Item-Thread").start();
    }
}
