package com.rockitgaming.pokemon.server.network.connections;

import com.rockitgaming.pokemon.server.Constants;
import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;
import org.jboss.netty.channel.Channel;

public class Session {

    private Boolean authenticated;
    private Channel channel;
    private String ipAddress;
    private Player player;

    public Session(Channel channel, String Ip) {
        this.channel = channel;
        authenticated = false;
        ipAddress = Ip;

        ServerMessage message = new ServerMessage(this);
        message.init(ClientPacket.SERVER_REVISION.getValue());
        message.addInt(Constants.REVISION);
        message.sendResponse();
    }

    public void close() {
        getChannel().close();
    }

    public Channel getChannel() {
        return channel;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Boolean getLoggedIn() {
        return authenticated;
    }

    public Player getPlayer() {
        return player;
    }

    public void parseMessage(ClientMessage msg) {
        /*
		 * If the player is logged in reset his lastPacket time for the
		 * idlekicker.
		 */
        if (player != null)
            player.lastPacket = System.currentTimeMillis();
        if (GameServer.getServiceManager().getGameRunnerService().getConnections().getMessages().contains(msg.getId()))
            GameServer.getServiceManager().getGameRunnerService().getConnections().getMessages().get(msg.getId())
                    .parse(this, msg, new ServerMessage(this));
    }

    public void Send(ServerMessage msg) {
        channel.write(msg);
    }

    public void setLoggedIn(boolean state) {
        authenticated = state;
    }

    public void setPlayer(Player player) {
        player.setSession(this);
        this.player = player;
    }
}
