package com.rockitgaming.pokemon.server.services;

import com.rockitgaming.pokemon.server.Constants;
import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.feature.battle.mechanics.statuses.field.FieldEffect;
import com.rockitgaming.pokemon.server.feature.battle.mechanics.statuses.field.HailEffect;
import com.rockitgaming.pokemon.server.feature.battle.mechanics.statuses.field.RainEffect;
import com.rockitgaming.pokemon.server.feature.battle.mechanics.statuses.field.SandstormEffect;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.Random;

/**
 * Handles game time and weather
 *
 * @author shadowkanji
 */
public class TimeService implements Runnable {

    private static final Log log = LogFactory.getLog(TimeService.class);

    /* NOTE: HAIL = SNOW */
    public enum Weather {
        FOG, HAIL, NORMAL, RAIN, SANDSTORM
    }

    private static int day = 0;
    private static int hour = 0;
    private static int minutes = 0;
    private static Weather weather = Weather.NORMAL;
    private int forcedWeather = 0;
    private boolean isRunning = true;
    private long lastWeatherUpdate = 0;
    private int idleKickTime = 10 * 60 * 1000;
    private int weatherUpdateTime = 15 * 60 * 1000;

    private Thread thread;

    /**
     * Default constructor
     */
    public TimeService() {
        generateWeather();
        lastWeatherUpdate = System.currentTimeMillis();
        thread = new Thread(this, "TimeService-Thread");
    }

    /**
     * Returns a string representation of the current time, e.g. 1201
     *
     * @return
     */
    public static String getTime() {
        return "" + (hour < 10 ? "0" + hour : hour) + (minutes < 10 ? "0" + minutes : minutes);
    }

    /**
     * Returns the field effect based on current weather
     *
     * @return
     */
    public static FieldEffect getWeatherEffect() {
        switch (weather) {
            case NORMAL:
                return null;
            case RAIN:
                return new RainEffect();
            case HAIL:
                return new HailEffect();
            case SANDSTORM:
                return new SandstormEffect();
            case FOG:
                return null;
            default:
                return null;
        }
    }

    /**
     * Returns the id of the weather
     *
     * @return
     */
    public static int getWeatherId() {
        switch (weather) {
            case NORMAL:
                return 0;
            case RAIN:
                return 1;
            case HAIL:
                return 2;
            case FOG:
                return 3;
            case SANDSTORM:
                return 4;
            default:
                return 0;
        }
    }

    /**
     * Returns true if it is night time
     *
     * @return
     */
    public static boolean isNight() {
        return hour > 19 || hour < 6;
    }

    /**
     * Returns the current Weather.
     *
     * @return
     */
    public int getForcedWeather() {
        return forcedWeather;
    }

    /**
     * Called by thread.start()
     */
    public void run() {
        Calendar cal = Calendar.getInstance();
        hour = cal.get(Calendar.HOUR_OF_DAY);
        minutes = cal.get(Calendar.MINUTE);
        day = cal.get(Calendar.DAY_OF_WEEK);

        while (isRunning) {

			/* Update the time. Time goes 6 times faster in Pokemon. */
            if (minutes == 59) {
                if (hour == 23) {
                    if (day == 6) {
                        day = 0;
                    } else {
                        day++;
                    }
                    hour = 0;
                } else {
                    hour++;
                }
                minutes = 0;
            } else {
                minutes++;
            }

			/* Check if weather should be updated every 15 minutes (in real time) */
            if (System.currentTimeMillis() - lastWeatherUpdate > weatherUpdateTime) {
                generateWeather();
                lastWeatherUpdate = System.currentTimeMillis();
            }
			/* Loop through all players and check for idling If they've idled, disconnect them. */
            for (Session session : ActiveConnections.allSessions().values()) {
                if (session.getPlayer() == null)
                    continue;
                Player player = session.getPlayer();
                if (System.currentTimeMillis() - idleKickTime > player.lastPacket) {
                    ServerMessage idleMessage = new ServerMessage(ClientPacket.SERVER_NOTIFICATION);
                    idleMessage.addString("You have been kicked for idling too long!");
                    session.Send(idleMessage);
                    ServerMessage login = new ServerMessage(ClientPacket.RETURN_TO_LOGIN);
                    session.Send(login);
                    GameServer.getServiceManager().getGameRunnerService().getLogoutManager().queuePlayer(player);
                    GameServer.getServiceManager().getMovementService().removePlayer(player.getName());
                    ActiveConnections.removeSession(session.getChannel());
                }
            }
            try {
                Thread.sleep(Constants.SLEEP_TIME);
            } catch (InterruptedException e) {
            }
        }
        log.info("Time Service stopped");
    }

    /**
     * Sets the weather.
     *
     * @return
     */
    public void setForcedWeather(int mForcedWeather) {
        forcedWeather = mForcedWeather;
        lastWeatherUpdate = 0;
    }

    /**
     * Stops this Time Service
     */
    public void stop() {
        isRunning = false;
    }

    /**
     * Generates a new weather status.
     * NOTE: Weather is generated randomly.
     */
    private void generateWeather() {
        int weather = forcedWeather;
        if (weather == 9)
            weather = new Random().nextInt(4);
        switch (weather) {
            case 0:
                this.weather = Weather.NORMAL;
                break;
            case 1:
                this.weather = Weather.RAIN;
                break;
            case 2:
                this.weather = Weather.HAIL;
                break;
            case 3:
                this.weather = Weather.FOG;
                break;
            case 4:
                this.weather = Weather.SANDSTORM;
                break;
            default:
                this.weather = Weather.NORMAL;
                break;
        }
    }
}
