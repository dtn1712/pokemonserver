package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class RegisterEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        try {
            GameServer.getServiceManager().getGameRunnerService().getRegistrationManager().register(session, request.readInt(), request.readString());
        } catch (Exception e) {
            e.printStackTrace();
            message.init(ClientPacket.REGISTER_ISSUES.getValue());
            message.addInt(3);
            session.Send(message);
        }
    }

}
