package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class SpriteEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {

        Player p = session.getPlayer();
        int sprite = request.readInt();
        /* Ensure the user buys a visible sprite */
        if (sprite > 0 && !GameServer.getServiceManager().getSpriteList().getUnbuyableSprites().contains(sprite))
            if (p.getMoney() >= 500) {
                p.setMoney(p.getMoney() - 500);
                p.updateClientMoney();
                p.setSprite(sprite);
                p.setSpriting(false);
            }

    }
}
