package com.rockitgaming.pokemon.server.feature;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Stores a database of pokemon caught by fishing
 *
 * @author shadowkanji
 * @author Fshy
 */
@Root
public class FishDatabase {

    private static final Log log = LogFactory.getLog(FishDatabase.class);

    @ElementMap
    private HashMap<String, ArrayList<FishPokemon>> m_database;

    /**
     * Adds an entry to the database
     *
     * @param pokemon
     * @param fishes
     */
    public void addEntry(String pokemon, ArrayList<FishPokemon> fishes) {
        if (m_database == null)
            m_database = new HashMap<>();
        m_database.put(pokemon, fishes);
    }

    /**
     * Removes an entry from the database
     *
     * @param pokemon
     */
    public void deleteEntry(String pokemon) {
        if (m_database == null) {
            m_database = new HashMap<>();
            return;
        }
        m_database.remove(pokemon);
    }

    public FishPokemon getFish(String pokemon) {
        pokemon = pokemon.toUpperCase();
        try {
            return m_database.get(pokemon).get(0);
        } catch (Exception e) {
            System.err.println("Pokemon: " + pokemon);
        }
        return m_database.get(pokemon).get(0);
    }

    /**
     * Reinitialises the database
     */
    public void reinitialize() {
        Thread t = new Thread(() -> {
            m_database = new HashMap<>();
            try {
                /* Read all the data from the text file */
                Scanner s = new Scanner(new File("./res/fishing.txt"));
                String pokemon = "";
                ArrayList<FishPokemon> fishies = new ArrayList<>();
                while (s.hasNextLine()) {
                    pokemon = s.nextLine();
                    fishies = new ArrayList<>();
                    /* parse the data in the order EXPERIENCE, LEVELREQ, RODREQ */
                    StringTokenizer st = new StringTokenizer(pokemon);
                    String pokeName = st.nextToken().toUpperCase();
                    while (st.hasMoreTokens()) {
                        int levelreq = Integer.parseInt(st.nextToken());
                        int exp = Integer.parseInt(st.nextToken());
                        int rodreq = Integer.parseInt(st.nextToken());
                        FishPokemon d = new FishPokemon(exp, levelreq, rodreq);
                        fishies.add(d);
                    }
                    addEntry(pokeName, fishies);
                }
                s.close();
                log.info("Fishing database reinitialized");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "FishDatabase_Thread");
        t.start();
    }
}
