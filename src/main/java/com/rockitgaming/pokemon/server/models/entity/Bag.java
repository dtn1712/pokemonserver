package com.rockitgaming.pokemon.server.models.entity;

import com.rockitgaming.pokemon.server.GameServer;

import java.util.ArrayList;

/**
 * Represents a player's bag
 */
public class Bag {

    private static final int BAG_SIZE = 600;

    private ArrayList<BagItem> items;
    private int memberId;

    /**
     * Default constructor
     */
    public Bag(int memberId) {
        this.memberId = memberId;
        this.items = new ArrayList<>();
    }

    /**
     * Adds an item to the bag. Returns true on success
     *
     * @param itemNumber
     * @param quantity
     */
    public boolean addItem(int itemNumber, int quantity) {
        int bagIndex = containsItem(itemNumber);
        if (bagIndex > -1) {
            items.get(bagIndex).increaseQuantity(quantity);
            return true;
        } else if (items.size() < BAG_SIZE) {
            items.add(new BagItem(itemNumber, quantity));
            return true;
        }
        return false;
    }

    /**
     * Checks if item is in bag. Returns bagIndex if true, else returns -1.
     *
     * @param itemNumber
     */
    public int containsItem(int itemNumber) {
        int bagIndex = -1;
        for (int i = 0; i < items.size(); i++)
            if (items.get(i).getItemNumber() == itemNumber) {
                return i;
            }
        return bagIndex;
    }

    /**
     * Checks if item is in bag. Returns quantity of items.
     *
     * @param itemNumber
     */
    public int getItemQuantity(int itemNumber) {
        int quantity = 0;
        for (int i = 0; i < items.size(); i++)
            if (items.get(i).getItemNumber() == itemNumber) {
                quantity = items.get(i).getQuantity();
                break;// End for loop. We found what we're looking for.
            }
        return quantity;
    }

    /**
     * Returns all the items in the bag
     */
    public ArrayList<BagItem> getItems() {
        return items;
    }

    /**
     * Returns this bag's member id
     *
     * @return
     */
    public int getMemberId() {
        return memberId;
    }

    /**
     * Returns true if there is space in the bag for that item
     *
     * @param itemId
     * @return
     */
    public boolean hasSpace(int itemId) {
        if (containsItem(itemId) >= 0 || items.size() < BAG_SIZE)
            return true;
        return false;
    }

    /**
     * Removes an item.
     *
     * @param itemNumber
     * @param quantity
     * @return Returns true if the item is succesfully removed, otherwise false.
     */
    public boolean removeItem(int itemNumber, int quantity) {
        for (int i = 0; i < items.size(); i++) {
            BagItem item = items.get(i);
            if (item.getItemNumber() == itemNumber) {
                if (item.getQuantity() - quantity > 0) {
                    item.decreaseQuantity(quantity);
                    GameServer.getServiceManager().getGameRunnerService().getSaveManager().updateBagItem(memberId,
                            item.getItemNumber(), item.getQuantity());
                } else {
                    items.remove(item);
                    GameServer.getServiceManager().getGameRunnerService().getSaveManager().removeBagItem(memberId,
                            item.getItemNumber());
                }
                return true;
            }
        }
        return false;
    }
}
