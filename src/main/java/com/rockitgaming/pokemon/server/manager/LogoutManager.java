package com.rockitgaming.pokemon.server.manager;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Handles logging players out
 */
public class LogoutManager implements Runnable {

    private static final Log log = LogFactory.getLog(LogoutManager.class);

    private DatabaseManager database;
    private boolean isRunning = false;
    private Queue<Player> logoutQueue;
    private SaveManager saveManager;
    private Thread thread;

    /**
     * Default constructor
     *
     * @param saveManager
     */
    public LogoutManager(SaveManager saveManager) {
        this.database = DatabaseManager.getInstance();
        this.logoutQueue = new LinkedList<>();
        this.saveManager = saveManager;
        this.thread = null;
    }

    /**
     * Returns how many players are in the save queue
     *
     * @return
     */
    public int getPlayerAmount() {
        return logoutQueue.size();
    }

    /**
     * Queues a player to be logged out
     *
     * @param player
     */
    public void queuePlayer(Player player) {
        if (thread == null || !thread.isAlive())
            start();
        if (!logoutQueue.contains(player)) {
            logoutQueue.offer(player);
        }
    }

    /**
     * Called by thread.start()
     */
    public void run() {
        while (isRunning) {
            synchronized (logoutQueue) {
                if (!logoutQueue.isEmpty()) {
                    Player player = logoutQueue.poll();
                    if (player != null) {
                        synchronized (player) {
                            if (!attemptLogout(player))
                                logoutQueue.add(player);
                            else {
                                player.dispose();
                                log.info(player.getName() + " logged out.");
                            }
                        }
                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
        thread = null;
        log.info("All player data saved successfully.");
    }

    /**
     * Start this logout manager
     */
    public void start() {
        if (thread == null || !thread.isAlive()) {
            thread = new Thread(this, "LogoutManager-Thread");
            isRunning = true;
            thread.start();
        }
    }

    public void stop() {
        isRunning = false;
    }

    /**
     * Attempts to logout a player by saving their data. Returns true on success
     *
     * @param player
     */
    private boolean attemptLogout(Player player) {
        /* Remove player from their map if it hasn't been done already. */
        if (player.getMap() != null) {
            player.getMap().removeChar(player);
        }

		/* TODO: Fix saving issues, issues may be players or pokemon. */
		/* Store all player information. */
        if (saveManager.savePlayer(player) > 0) {
            return false;
        }

		/* Finally, store that the player is logged out and close connection. */
        database.query("UPDATE `pn_members` SET `lastLoginServer` = 'null' WHERE `id` = '" + player.getId() + "'");
        GameServer.getServiceManager().getMovementService().removePlayer(player.getName());
        ActiveConnections.removeSession(player.getSession().getChannel());
//        GameServer.getInstance().updatePlayerCount();

        return true;
    }
}
