package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Positionable.Direction;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class MoveUpEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        session.getPlayer().move(Direction.Up);
    }

}
