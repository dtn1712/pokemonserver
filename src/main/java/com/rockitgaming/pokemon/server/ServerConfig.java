package com.rockitgaming.pokemon.server;

import org.springframework.util.Assert;

import java.util.Properties;

public class ServerConfig {

	public static String serverIp;
	public static int serverPort;

	// Netty
	public static int connectTimeoutMillis;
	public static int soBackLog;
	public static boolean soKeepAlive;
	public static int systemMsgThreadPoolSize;
	public static int msgThreadPoolSize;

	// Redis cache
	public static String redisCacheConnection;

	public static int threadCorePoolSize;
	public static int threadMaxPoolSize;
	public static int threadQueueCapacity;

	// config logic game
	public static int delayTimeMillis;
	public static int maxPlayers = 500;

	// for timer schedule
	public static int serverId;
	public static int delayLoginTime;
	public static int userCountPerPage;
	public static Properties properties;


	public static void init(Properties prop) {
		Assert.notNull(prop);

		serverIp = prop.getProperty("server.ip", "127.0.0.1");
		serverPort = Integer.parseInt(prop.getProperty("server.port", "8000"));
		soBackLog = Integer.parseInt(prop.getProperty("soBacklog", "1000"));
		connectTimeoutMillis = Integer.parseInt(prop.getProperty("connectTimeoutMillis", "30000"));
		soKeepAlive = Boolean.parseBoolean(prop.getProperty("soKeepAlive", "true"));
		systemMsgThreadPoolSize = Integer.parseInt(prop.getProperty("systemMessageExecutor.threadPoolSize", "1"));
		msgThreadPoolSize = Integer.parseInt(prop.getProperty("messageExecutor.threadPoolSize", "1"));

		// redis
		redisCacheConnection = prop.getProperty("redis.cacheLocation");

		threadCorePoolSize = Integer.parseInt(prop.getProperty("threadCorePoolSize"));
		threadMaxPoolSize = Integer.parseInt(prop.getProperty("threadMaxPoolSize"));
		threadQueueCapacity = Integer.parseInt(prop.getProperty("threadQueueCapacity"));

		// game config
		delayTimeMillis = Integer.parseInt(prop.getProperty("delayTimeMillis", "1000"));
		delayLoginTime = Integer.parseInt(prop.getProperty("delayLoginTime"));
		serverId = Integer.parseInt(prop.getProperty("serverId"));
		userCountPerPage = Integer.parseInt(prop.getProperty("userCountPerPage"));
		maxPlayers = Integer.parseInt(prop.getProperty("maxPlayers","500"));

		properties = new Properties();
		properties.putAll(prop);
	}

}
