package com.rockitgaming.pokemon.server.feature;

import org.simpleframework.xml.Element;

/**
 * Stores exp and level information for a Fish Pokemon(one found by fishing)
 *
 * @author Fshy
 */
public class FishPokemon {

    @Element
    private int experience;
    @Element
    private int reqLevel;
    @Element
    private int reqRod;

    public FishPokemon(){}

    public FishPokemon(int experience, int reqLevel, int reqRod) {
        this.experience = experience;
        this.reqLevel = reqLevel;
        this.reqRod = reqRod;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getReqLevel() {
        return reqLevel;
    }

    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }

    public int getReqRod() {
        return reqRod;
    }

    public void setReqRod(int reqRod) {
        this.reqRod = reqRod;
    }
}
