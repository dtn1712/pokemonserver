package com.rockitgaming.pokemon.server.network.connections;

import com.rockitgaming.pokemon.server.ServerConfig;
import com.rockitgaming.pokemon.server.manager.LogoutManager;
import com.rockitgaming.pokemon.server.network.messages.MessageHandler;
import com.rockitgaming.pokemon.server.network.protocol.codec.NetworkDecoder;
import com.rockitgaming.pokemon.server.network.protocol.codec.NetworkEncoder;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.frame.LengthFieldPrepender;

import java.net.InetSocketAddress;

public class SocketConnection {
    private ServerBootstrap serverBootstrap;
    private NioServerSocketChannelFactory socketFactory;
    private LogoutManager logoutManager;
    private MessageHandler messages;

    public SocketConnection(LogoutManager logoutManager) {
        this.logoutManager = logoutManager;
        this.socketFactory = new NioServerSocketChannelFactory();
        this.serverBootstrap = new ServerBootstrap(socketFactory);
        this.messages = new MessageHandler();
        this.messages.register();
        setupSocket();
    }

    public void start() {
        serverBootstrap.bind(new InetSocketAddress(ServerConfig.serverIp, ServerConfig.serverPort));
    }

    public void stop() {
        socketFactory.shutdown();
        serverBootstrap.shutdown();
    }

    public MessageHandler getMessages() {
        return messages;
    }

    private void setupSocket() {
        ChannelPipeline pipeline = serverBootstrap.getPipeline();
        pipeline.addLast("lengthEncoder", new LengthFieldPrepender(4));
        pipeline.addLast("encoder", new NetworkEncoder());
        pipeline.addLast("decoder", new NetworkDecoder());
        pipeline.addLast("handler", new SocketConnectionHandler(logoutManager));
    }
}
