package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class PlayerLastPosition implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        String[] details = request.readString().split(",");
        session.getPlayer().setLastX(Integer.parseInt(details[0]));
        session.getPlayer().setLastY(Integer.parseInt(details[1]));
    }

}
