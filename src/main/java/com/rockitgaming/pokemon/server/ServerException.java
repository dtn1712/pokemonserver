package com.rockitgaming.pokemon.server;

/**
 * Created by dangn on 7/11/16.
 */
public class ServerException extends Exception {

    public ServerException(String message) {
        super(message);
    }

    public ServerException(String message, Throwable cause) {
        super(message,cause);
    }

    public ServerException(Throwable cause) {
        super(cause);
    }
}
