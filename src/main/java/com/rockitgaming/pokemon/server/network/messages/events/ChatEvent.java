package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.models.map.ServerMap;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class ChatEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player player = session.getPlayer();
        int type = request.readInt();
        String msg = request.readString();
        switch (type) {
            case 0: // local
                if (!player.isMuted()) {
                    ServerMap map = GameServer.getServiceManager().getMovementService().getMapMatrix().getMapByGamePosition(player.getMapX(), player.getMapY());
                    if (map != null)
                        map.sendChatMessage("<" + player.getName() + "> " + msg, player.getLanguage());
                }
                break;
            case 1: // global
                if (!player.isMuted()) {
                    for (Session ses : ActiveConnections.allSessions().values())
                        if (ses.getPlayer() != null) {
                            ServerMessage globalChat = new ServerMessage(ClientPacket.CHAT_PACKET);
                            globalChat.addInt(0);
                            globalChat.addString("<" + player.getName() + "> " + msg);
                            ses.Send(globalChat);
                        }
                }
            case 2: // private
                String[] details = msg.split(",");
                String targetPlayer = details[0];
                Player target = ActiveConnections.getPlayer(targetPlayer);
                if (target != null) {
                    ServerMessage targetMessage = new ServerMessage(ClientPacket.CHAT_PACKET);
                    targetMessage.addInt(1);
                    targetMessage.addString(player.getName() + "," + "<" + player.getName() + "> " + details[1]);
                    target.getSession().Send(targetMessage);
                }
                break;
        }
    }
}
