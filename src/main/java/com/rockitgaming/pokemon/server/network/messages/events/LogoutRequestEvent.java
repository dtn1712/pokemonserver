package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.feature.battle.impl.PvPBattleField;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class LogoutRequestEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        try {
            Player player = session.getPlayer();
            if (player.isBattling()) {
                /* If in PvP battle, the player loses */
                if (player.getBattleField() instanceof PvPBattleField) {
                    ((PvPBattleField) player.getBattleField()).disconnect(player.getBattleId());
                }
                player.setBattleField(null);
                player.setBattling(false);
                player.lostBattle();
            }
			/* If trading, end the trade */
            if (player.isTrading()) {
                player.getTrade().endTrade();
            }
            GameServer.getServiceManager().getGameRunnerService().getLogoutManager().queuePlayer(player);
            GameServer.getServiceManager().getMovementService().removePlayer(player.getName());
            ActiveConnections.removeSession(session.getChannel());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
