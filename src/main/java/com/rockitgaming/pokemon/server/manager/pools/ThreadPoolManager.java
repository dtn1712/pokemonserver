package com.rockitgaming.pokemon.server.manager.pools;

import com.rockitgaming.pokemon.server.ServerConfig;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadPoolManager {

	private ThreadPoolTaskExecutor pool;
	
	private static ThreadPoolManager instance;
	
	public static ThreadPoolManager getInstance() {
		if (instance == null) {
			instance = new ThreadPoolManager();
		}
		return instance;
	}

	
	private ThreadPoolManager() {
		pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(ServerConfig.threadCorePoolSize);
		pool.setMaxPoolSize(ServerConfig.threadMaxPoolSize);
		pool.setQueueCapacity(ServerConfig.threadQueueCapacity);
		pool.initialize();
	}
	
	public void shutdown() {
		pool.shutdown();
	}

	public ThreadPoolTaskExecutor getPool() {
		return pool;
	}

	public void execute(Runnable task) {
		pool.execute(task);
	}

	
}
