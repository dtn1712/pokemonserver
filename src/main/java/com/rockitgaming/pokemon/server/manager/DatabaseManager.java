package com.rockitgaming.pokemon.server.manager;

import com.rockitgaming.pokemon.server.Constants;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Handles database transaction.
 */
public class DatabaseManager {

    private static final Log log = LogFactory.getLog(DatabaseManager.class);

    private static DatabaseManager instance;

    private HikariConfig config;
    private HikariDataSource dataSource;
    private Connection connection;

    private DatabaseManager() {
        config = new HikariConfig(Constants.DATABASE_CONFIG_FILE_NAME);
        dataSource = new HikariDataSource(config);
    }

    public static DatabaseManager getInstance() {
        if (instance == null)
            instance = new DatabaseManager();
        return instance;
    }

    public Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = dataSource.getConnection();
        }
        return connection;
    }

    public static String parseSQL(String text) {
        if (text == null)
            text = "";
        text = text.replace("'", "''");
        text = text.replace("\\", "\\\\");
        return text;
    }

    public ResultSet query(String query) {
        PreparedStatement stmt;

        if (query.startsWith("SELECT")) {
            try {
                Connection connection = getConnection();
                stmt = connection.prepareStatement(query);
                ResultSet queryResult = stmt.executeQuery();
                log.info(String.format("Execute query %s successfully",query));
                return queryResult;
            } catch (SQLException e) {
                log.error(String.format("Failed to execute query %s",query),e);
            }
        } else {
            try {
                Connection connection = getConnection();
                stmt = connection.prepareStatement(query);
                stmt.executeUpdate();
                log.info(String.format("Execute query %s successfully",query));
            } catch (SQLException e) {
                log.error(String.format("Failed to execute query %s",query),e);
            }
        }
        return null;
    }

    public void close() {
        if (!dataSource.isClosed()) {
            dataSource.close();
        }
        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("Failed to close connection",e);
        }
    }

}
