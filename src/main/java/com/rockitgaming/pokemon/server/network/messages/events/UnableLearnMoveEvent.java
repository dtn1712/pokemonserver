package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class UnableLearnMoveEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        // Player is allowing move to be learned
        Player p = session.getPlayer();
        int pokemonIndex = request.readInt();
        String move = request.readString();

        if (p.getParty()[pokemonIndex] != null)
            if (p.getParty()[pokemonIndex].getMovesLearning().contains(move))
                p.getParty()[pokemonIndex].getMovesLearning().remove(move);
    }

}
