﻿/*
Navicat MySQL Data Transfer

Source Server         : ragnarok
Source Server Version : 50140
Source Host           : localhost:3306
Source Database       : pokemon

Target Server Type    : MYSQL
Target Server Version : 50140
File Encoding         : 65001

Date: 2015-01-17 20:56:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `pn_bag`
-- ----------------------------
DROP TABLE IF EXISTS `pn_bag`;
CREATE TABLE `pn_bag` (
  `member` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`member`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of pn_bag
-- ----------------------------
INSERT INTO `pn_bag` VALUES ('7', '35', '5');
INSERT INTO `pn_bag` VALUES ('8', '35', '5');
INSERT INTO `pn_bag` VALUES ('9', '35', '5');
INSERT INTO `pn_bag` VALUES ('10', '35', '5');
INSERT INTO `pn_bag` VALUES ('10', '206', '1');
INSERT INTO `pn_bag` VALUES ('11', '1', '6');
INSERT INTO `pn_bag` VALUES ('11', '35', '2');
INSERT INTO `pn_bag` VALUES ('11', '202', '1');
INSERT INTO `pn_bag` VALUES ('11', '206', '4');
INSERT INTO `pn_bag` VALUES ('12', '35', '28');
INSERT INTO `pn_bag` VALUES ('12', '97', '2');
INSERT INTO `pn_bag` VALUES ('12', '168', '3');
INSERT INTO `pn_bag` VALUES ('12', '606', '1');
INSERT INTO `pn_bag` VALUES ('13', '35', '23');
INSERT INTO `pn_bag` VALUES ('13', '200', '1');
INSERT INTO `pn_bag` VALUES ('13', '202', '8');
INSERT INTO `pn_bag` VALUES ('13', '206', '24');
INSERT INTO `pn_bag` VALUES ('13', '207', '5');
INSERT INTO `pn_bag` VALUES ('14', '1', '10');
INSERT INTO `pn_bag` VALUES ('14', '2', '3');
INSERT INTO `pn_bag` VALUES ('14', '35', '2');
INSERT INTO `pn_bag` VALUES ('14', '36', '6');
INSERT INTO `pn_bag` VALUES ('14', '85', '26');
INSERT INTO `pn_bag` VALUES ('14', '91', '18');
INSERT INTO `pn_bag` VALUES ('14', '200', '2');
INSERT INTO `pn_bag` VALUES ('14', '202', '6');
INSERT INTO `pn_bag` VALUES ('14', '203', '2');
INSERT INTO `pn_bag` VALUES ('14', '206', '59');
INSERT INTO `pn_bag` VALUES ('14', '207', '24');
INSERT INTO `pn_bag` VALUES ('14', '209', '4');
INSERT INTO `pn_bag` VALUES ('14', '353', '1');
INSERT INTO `pn_bag` VALUES ('14', '483', '1');
INSERT INTO `pn_bag` VALUES ('14', '603', '1');
INSERT INTO `pn_bag` VALUES ('15', '35', '5');
INSERT INTO `pn_bag` VALUES ('16', '2', '4');
INSERT INTO `pn_bag` VALUES ('16', '36', '14');
INSERT INTO `pn_bag` VALUES ('16', '168', '1');
INSERT INTO `pn_bag` VALUES ('16', '200', '1');
INSERT INTO `pn_bag` VALUES ('16', '206', '30');
INSERT INTO `pn_bag` VALUES ('16', '610', '1');
INSERT INTO `pn_bag` VALUES ('17', '2', '4');
INSERT INTO `pn_bag` VALUES ('17', '35', '5');
INSERT INTO `pn_bag` VALUES ('17', '36', '5');
INSERT INTO `pn_bag` VALUES ('17', '85', '10');
INSERT INTO `pn_bag` VALUES ('17', '200', '1');
INSERT INTO `pn_bag` VALUES ('17', '206', '16');
INSERT INTO `pn_bag` VALUES ('18', '1', '2');
INSERT INTO `pn_bag` VALUES ('18', '35', '5');
INSERT INTO `pn_bag` VALUES ('18', '36', '14');
INSERT INTO `pn_bag` VALUES ('18', '202', '1');
INSERT INTO `pn_bag` VALUES ('18', '206', '12');
INSERT INTO `pn_bag` VALUES ('18', '207', '9');
INSERT INTO `pn_bag` VALUES ('19', '35', '5');
INSERT INTO `pn_bag` VALUES ('20', '35', '5');
INSERT INTO `pn_bag` VALUES ('20', '200', '5');
INSERT INTO `pn_bag` VALUES ('20', '201', '1');
INSERT INTO `pn_bag` VALUES ('20', '204', '1');
INSERT INTO `pn_bag` VALUES ('20', '206', '10');

-- ----------------------------
-- Table structure for `pn_bans`
-- ----------------------------
DROP TABLE IF EXISTS `pn_bans`;
CREATE TABLE `pn_bans` (
  `playername` varchar(12) NOT NULL,
  `ip` varchar(16) NOT NULL,
  PRIMARY KEY (`playername`,`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_bans
-- ----------------------------
INSERT INTO `pn_bans` VALUES ('esmeralda', '186.130.37.87');

-- ----------------------------
-- Table structure for `pn_box`
-- ----------------------------
DROP TABLE IF EXISTS `pn_box`;
CREATE TABLE `pn_box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member` int(11) DEFAULT NULL,
  `pokemon0` int(11) DEFAULT NULL,
  `pokemon1` int(11) DEFAULT NULL,
  `pokemon2` int(11) DEFAULT NULL,
  `pokemon3` int(11) DEFAULT NULL,
  `pokemon4` int(11) DEFAULT NULL,
  `pokemon5` int(11) DEFAULT NULL,
  `pokemon6` int(11) DEFAULT NULL,
  `pokemon7` int(11) DEFAULT NULL,
  `pokemon8` int(11) DEFAULT NULL,
  `pokemon9` int(11) DEFAULT NULL,
  `pokemon10` int(11) DEFAULT NULL,
  `pokemon11` int(11) DEFAULT NULL,
  `pokemon12` int(11) DEFAULT NULL,
  `pokemon13` int(11) DEFAULT NULL,
  `pokemon14` int(11) DEFAULT NULL,
  `pokemon15` int(11) DEFAULT NULL,
  `pokemon16` int(11) DEFAULT NULL,
  `pokemon17` int(11) DEFAULT NULL,
  `pokemon18` int(11) DEFAULT NULL,
  `pokemon19` int(11) DEFAULT NULL,
  `pokemon20` int(11) DEFAULT NULL,
  `pokemon21` int(11) DEFAULT NULL,
  `pokemon22` int(11) DEFAULT NULL,
  `pokemon23` int(11) DEFAULT NULL,
  `pokemon24` int(11) DEFAULT NULL,
  `pokemon25` int(11) DEFAULT NULL,
  `pokemon26` int(11) DEFAULT NULL,
  `pokemon27` int(11) DEFAULT NULL,
  `pokemon28` int(11) DEFAULT NULL,
  `pokemon29` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_box_idx` (`member`),
  KEY `first_poke_idx` (`pokemon0`),
  CONSTRAINT `player_box_fk` FOREIGN KEY (`member`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_box
-- ----------------------------

-- ----------------------------
-- Table structure for `pn_cheatlog`
-- ----------------------------
DROP TABLE IF EXISTS `pn_cheatlog`;
CREATE TABLE `pn_cheatlog` (
  `playerid` int(11) NOT NULL,
  `date` varchar(25) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`playerid`,`date`),
  KEY `player_cheat_idx` (`playerid`),
  CONSTRAINT `player_cheat_fk` FOREIGN KEY (`playerid`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_cheatlog
-- ----------------------------

-- ----------------------------
-- Table structure for `pn_friends`
-- ----------------------------
DROP TABLE IF EXISTS `pn_friends`;
CREATE TABLE `pn_friends` (
  `id` int(11) NOT NULL,
  `friendId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`friendId`),
  KEY `player_friend_idx` (`id`),
  KEY `friend_idx` (`friendId`),
  CONSTRAINT `firend_fk` FOREIGN KEY (`friendId`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `player_friend_fk` FOREIGN KEY (`id`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_friends
-- ----------------------------

-- ----------------------------
-- Table structure for `pn_members`
-- ----------------------------
DROP TABLE IF EXISTS `pn_members`;
CREATE TABLE `pn_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dob` varchar(12) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `lastLoginTime` varchar(42) DEFAULT NULL,
  `lastLoginServer` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `lastLoginIP` varchar(24) DEFAULT NULL,
  `lastLanguageUsed` tinyint(4) DEFAULT NULL,
  `sprite` smallint(6) DEFAULT NULL,
  `party` int(11) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `npcMul` varchar(6) DEFAULT NULL,
  `skHerb` int(11) DEFAULT NULL,
  `skCraft` int(11) DEFAULT NULL,
  `skFish` int(11) DEFAULT NULL,
  `skTrain` int(11) DEFAULT NULL,
  `skCoord` int(11) DEFAULT NULL,
  `skBreed` int(11) DEFAULT NULL,
  `x` smallint(6) DEFAULT NULL,
  `y` smallint(6) DEFAULT NULL,
  `mapX` tinyint(4) DEFAULT NULL,
  `mapY` tinyint(4) DEFAULT NULL,
  `bag` int(11) DEFAULT NULL,
  `badges` varchar(50) DEFAULT NULL,
  `healX` int(11) DEFAULT NULL,
  `healY` int(11) DEFAULT NULL,
  `healMapX` int(11) DEFAULT NULL,
  `healMapY` int(11) DEFAULT NULL,
  `isSurfing` varchar(5) DEFAULT NULL,
  `adminLevel` tinyint(3) unsigned DEFAULT NULL,
  `muted` varchar(5) DEFAULT '',
  `pokedexId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username_idx` (`username`),
  KEY `dex_id_idx` (`pokedexId`),
  CONSTRAINT `dex_id` FOREIGN KEY (`pokedexId`) REFERENCES `pn_pokedex` (`pokedexId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_members
-- ----------------------------
INSERT INTO `pn_members` VALUES ('7', 'Rabbit', '5485054D8E695E3937AAB2B9ED25BD4E5E4B96A7367C0DD34E3A28F15529C4CF166144853FDEAF8D8C7329EB9B0E94F26B9BC0173724091CA7F4BAD238073BD6', '19/11/1991', 'conejo@hotmail.com', '1411001019121', 'null', '190.179.166.5', '5', '11', '7', '1000', null, '0', '0', '0', '0', '0', '0', '480', '312', '3', '1', null, '000000000000000000000000000000000000000000', '512', '440', '3', '1', 'false', '0', 'false', '7');
INSERT INTO `pn_members` VALUES ('8', 'conejoo', 'D8A1DF1061EE535737033D413401B09DFB5311C9B9E0E10F5E81E2ED1DD89EA297940106FE15896B96EF9721974E48903745D79AC279ADA669382AA4F66460AA', '19/11/1991', 'conejoo@hotmail.com', '0', 'null', null, null, '11', '8', '1000', null, '0', '0', '0', '0', '0', '0', '512', '440', '3', '1', null, '00000000000000000000000000000000000000000000000000', '512', '440', '3', '1', 'false', '0', 'false', '8');
INSERT INTO `pn_members` VALUES ('9', 'snupy', 'C6FA7EA0869822AF694C319A8F915F6CD2EBC35F79706A72499355589CDD0E41ADBD99D59164C36B29E6327983D6D846948AF2EA8519A0E68BE8C86B78C2E90A', '19/11/1991', 'f3re@hotmail.com', '1411021396439', 'null', '190.179.166.5', '5', '11', '9', '1100', null, '0', '0', '0', '35', '0', '0', '512', '344', '3', '1', null, '000000000000000000000000000000000000000000', '512', '440', '3', '1', 'false', '0', 'false', '9');
INSERT INTO `pn_members` VALUES ('10', 'tumacho', '2B0DC49589F5B1E8A6E12ABDFFC0E027C4D0E9CDE56F9ED253C7E05D6DCF1546381B4556D3C09356B4FCB7010CCE66C187FE8D0177A52ABDC00139CE1B05EF73', '19/11/1991', 'tumacho@hotmail.com', '1411022345463', 'null', '190.179.166.5', '5', '11', '10', '1200', null, '0', '0', '0', '65', '0', '0', '96', '216', '3', '1', null, '000000000000000000000000000000000000000000', '512', '440', '3', '1', 'false', '0', 'false', '10');
INSERT INTO `pn_members` VALUES ('11', 'probando', '07E9BC5E94584E96596707C7B0AD3EA42FD817EC1355DA6DCE697DC372357986D309B09A79CC32EDC9AE9B19B6BECA5A725AD5A18C957902256E743732813AF8', '19/11/1991', 'probando@hotmail.com', '1411325911143', 'null', '186.130.37.87', '5', '11', '11', '11100', null, '0', '0', '0', '2463', '0', '0', '320', '312', '3', '-2', null, '100000000000000000000000000000000000000000', '480', '376', '-49', '-29', 'false', '0', 'false', '11');
INSERT INTO `pn_members` VALUES ('12', 'Teepo', '07AF5B65E9A95C9EA9738F697A08206C7E6D3AFC4C1C7E7C95BC1E1D94769C8DAD7ADDB4551C3A1A0770E794B68FEDD6EC50A0E0870B7F5C758A87577AD02E47', '11/09/1992', 'luigi_thun@live.com', '1412789675685', 'null', '151.16.61.139', '0', '195', '12', '153410', null, '0', '0', '90', '39779', '0', '0', '192', '120', '3', '-2', null, '111000100010000000000000000000000000000000', '480', '376', '-49', '-29', 'false', '3', 'false', '12');
INSERT INTO `pn_members` VALUES ('13', 'mapplebox', '41C5BFFA8C04EF95BD38BC3D27553836D19BDC3682A042657EE4DCAAD474C58CB99A05D1126D14A99CC049A20E3CB811BFBA422064FD0F35901DCBD6A600A14E', '28/10/1997', 'lion.glotz@yahoo.de', '1412020408802', 'null', '84.159.221.212', '7', '226', '13', '62500', null, '0', '0', '0', '8400', '0', '0', '480', '408', '-49', '-41', null, '000000001000000000000000000000000000000000', '480', '376', '-49', '-41', 'false', '0', 'false', '13');
INSERT INTO `pn_members` VALUES ('14', 'Matz115', '1406136B91404F7BAAC93E5DD2C1B3EAC93D294DA6840436D0336018322DD649939614348455458F9CC10E297828CB1D1E63D0719E518CD649A0C044D96E8BA0', '16/08/1994', 'Matz115@live.com', '1412291374344', 'null', '99.244.58.180', '0', '348', '14', '160650', null, '0', '0', '0', '166627', '0', '0', '160', '88', '-46', '-29', null, '111110101111011000000000000000000000000000', '480', '376', '-46', '-31', 'false', '0', 'false', '14');
INSERT INTO `pn_members` VALUES ('15', 'Wobble', 'E098937D5046E3D77C522A69A9E461B18592EE01CE07E8BF8DD9D0273D9CE88E28146EB4AEA32ED2D4A5F288F03EF96F6ABEBCAAA11B0F581A7D8693BC482B57', '28/11/1999', 'jeromeholznagel@web.de', '1411325650033', 'null', '188.103.64.92', '7', '11', '15', '1300', null, '0', '0', '0', '30', '0', '0', '1280', '792', '-1', '0', null, '000000000000000000000000000000000000000000', '640', '568', '-49', '-47', 'false', '0', 'false', '15');
INSERT INTO `pn_members` VALUES ('16', 'Noah', '4BDF1170DB1671F3994F23282AE091EAF817AAC75F064D2D479E5D03F2CE7F7181E9F6877B100A7A067962C276C5C454029D6E455C043542746B3B111F0245F9', '09/28/1993', 'me@naterandall.com', '1412275421112', 'null', '73.176.121.21', '0', '305', '16', '26500', null, '0', '0', '0', '58590', '0', '0', '544', '1240', '-5', '-1', null, '111110110010000000000000000000000000000000', '480', '376', '-49', '-41', 'false', '0', 'false', '16');
INSERT INTO `pn_members` VALUES ('17', 'esmeralda', '9C9709B8B563891ED0FC0AEB68DF3FAFA492E4E078ACB87F552440CA59D2496E5E39BA659927F3A759209DEE9E90E264C67FBBF66127FD7D98200CB34F01D414', '19/11/1991', 'esmeralda@hotmail.com', '1412839957013', 'null', '190.179.162.212', '0', '117', '17', '22600', null, '0', '0', '0', '19249', '0', '0', '704', '664', '6', '-4', null, '110000000000000000000000000000000000000000', '480', '376', '-48', '-30', 'false', '5', 'false', '17');
INSERT INTO `pn_members` VALUES ('18', 'fernamaton', '8E5C81CCDF163B1C440FD11577DE90FB408388F53B7248B4325966B45218FED27BB416DD27A33B71B2A02287A0101355C197EE8FEBE04570886CE34A3B428198', '09/11/1991', 'fernamaton@gmail.com', '1411620253786', 'null', '190.238.8.88', '5', '11', '18', '27350', null, '0', '0', '0', '4525', '0', '0', '320', '2168', '3', '-2', null, '000000000000000000000000000000000000000000', '480', '376', '-49', '-29', 'false', '0', 'false', '18');
INSERT INTO `pn_members` VALUES ('19', 'Yokil', '1E107E2602815B672411A904CD39898BCD40D890BE2CCA269C7EB89E959D59F8C097F109105AF7D8003086CBA065329EAF5B4F95AA66FC88081774DA7BB4C00C', '08/04/1990', 'apvsalvado@hotmail.com', '1411361811129', 'null', '187.113.106.244', '0', '11', '19', '1300', null, '0', '0', '0', '30', '0', '0', '320', '216', '3', '1', null, '000000000000000000000000000000000000000000', '512', '440', '3', '1', 'false', '0', 'false', '19');
INSERT INTO `pn_members` VALUES ('20', 'Zorro', '25B363186A1C1D5E387E1ED8B68A1D08FBF2CE6B0D1C13BB335E51A6F2CE57E149EFF926F2C179ADB7DC8394BD6532AD726EC6696F6961B0D6D74EB3D2C69AD8', '12/09/1991', 'shuusui@web.de', '1412757884544', 'null', '134.3.127.92', '0', '11', '20', '21000', null, '0', '0', '0', '1555', '0', '0', '704', '568', '-3', '-1', null, '000000000000000000000000000000000000000000', '480', '376', '-49', '-38', 'false', '0', 'false', '20');

-- ----------------------------
-- Table structure for `pn_mypokes`
-- ----------------------------
DROP TABLE IF EXISTS `pn_mypokes`;
CREATE TABLE `pn_mypokes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member` int(11) DEFAULT NULL,
  `party` int(11) DEFAULT NULL,
  `box0` int(11) DEFAULT NULL,
  `box1` int(11) DEFAULT NULL,
  `box2` int(11) DEFAULT NULL,
  `box3` int(11) DEFAULT NULL,
  `box4` int(11) DEFAULT NULL,
  `box5` int(11) DEFAULT NULL,
  `box6` int(11) DEFAULT NULL,
  `box7` int(11) DEFAULT NULL,
  `box8` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_pokes_idx` (`member`),
  KEY `party_idx` (`party`),
  CONSTRAINT `party_pokes_fk` FOREIGN KEY (`party`) REFERENCES `pn_party` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `player_pokes_fk` FOREIGN KEY (`member`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_mypokes
-- ----------------------------

-- ----------------------------
-- Table structure for `pn_party`
-- ----------------------------
DROP TABLE IF EXISTS `pn_party`;
CREATE TABLE `pn_party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member` int(11) DEFAULT NULL,
  `pokemon0` int(11) DEFAULT NULL,
  `pokemon1` int(11) DEFAULT NULL,
  `pokemon2` int(11) DEFAULT NULL,
  `pokemon3` int(11) DEFAULT NULL,
  `pokemon4` int(11) DEFAULT NULL,
  `pokemon5` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_party_idx` (`member`),
  CONSTRAINT `player_party_fk` FOREIGN KEY (`member`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_party
-- ----------------------------
INSERT INTO `pn_party` VALUES ('7', '7', '14', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('8', '8', '15', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('9', '9', '16', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('10', '10', '17', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('11', '11', '18', '19', '20', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('12', '12', '21', '40', '34', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('13', '13', '23', '24', '31', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('14', '14', '48', '25', '37', '33', '32', '-1');
INSERT INTO `pn_party` VALUES ('15', '15', '26', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('16', '16', '27', '29', '47', '46', '-1', '-1');
INSERT INTO `pn_party` VALUES ('17', '17', '30', '49', '50', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('18', '18', '43', '35', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('19', '19', '36', '-1', '-1', '-1', '-1', '-1');
INSERT INTO `pn_party` VALUES ('20', '20', '51', '-1', '-1', '-1', '-1', '-1');

-- ----------------------------
-- Table structure for `pn_pokedex`
-- ----------------------------
DROP TABLE IF EXISTS `pn_pokedex`;
CREATE TABLE `pn_pokedex` (
  `pokedexId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `1` tinyint(4) NOT NULL,
  `2` tinyint(4) NOT NULL,
  `3` tinyint(4) NOT NULL,
  `4` tinyint(4) NOT NULL,
  `5` tinyint(4) NOT NULL,
  `6` tinyint(4) NOT NULL,
  `7` tinyint(4) NOT NULL,
  `8` tinyint(4) NOT NULL,
  `9` tinyint(4) NOT NULL,
  `10` tinyint(4) NOT NULL,
  `11` tinyint(4) NOT NULL,
  `12` tinyint(4) NOT NULL,
  `13` tinyint(4) NOT NULL,
  `14` tinyint(4) NOT NULL,
  `15` tinyint(4) NOT NULL,
  `16` tinyint(4) NOT NULL,
  `17` tinyint(4) NOT NULL,
  `18` tinyint(4) NOT NULL,
  `19` tinyint(4) NOT NULL,
  `20` tinyint(4) NOT NULL,
  `21` tinyint(4) NOT NULL,
  `22` tinyint(4) NOT NULL,
  `23` tinyint(4) NOT NULL,
  `24` tinyint(4) NOT NULL,
  `25` tinyint(4) NOT NULL,
  `26` tinyint(4) NOT NULL,
  `27` tinyint(4) NOT NULL,
  `28` tinyint(4) NOT NULL,
  `29` tinyint(4) NOT NULL,
  `30` tinyint(4) NOT NULL,
  `31` tinyint(4) NOT NULL,
  `32` tinyint(4) NOT NULL,
  `33` tinyint(4) NOT NULL,
  `34` tinyint(4) NOT NULL,
  `35` tinyint(4) NOT NULL,
  `36` tinyint(4) NOT NULL,
  `37` tinyint(4) NOT NULL,
  `38` tinyint(4) NOT NULL,
  `39` tinyint(4) NOT NULL,
  `40` tinyint(4) NOT NULL,
  `41` tinyint(4) NOT NULL,
  `42` tinyint(4) NOT NULL,
  `43` tinyint(4) NOT NULL,
  `44` tinyint(4) NOT NULL,
  `45` tinyint(4) NOT NULL,
  `46` tinyint(4) NOT NULL,
  `47` tinyint(4) NOT NULL,
  `48` tinyint(4) NOT NULL,
  `49` tinyint(4) NOT NULL,
  `50` tinyint(4) NOT NULL,
  `51` tinyint(4) NOT NULL,
  `52` tinyint(4) NOT NULL,
  `53` tinyint(4) NOT NULL,
  `54` tinyint(4) NOT NULL,
  `55` tinyint(4) NOT NULL,
  `56` tinyint(4) NOT NULL,
  `57` tinyint(4) NOT NULL,
  `58` tinyint(4) NOT NULL,
  `59` tinyint(4) NOT NULL,
  `60` tinyint(4) NOT NULL,
  `61` tinyint(4) NOT NULL,
  `62` tinyint(4) NOT NULL,
  `63` tinyint(4) NOT NULL,
  `64` tinyint(4) NOT NULL,
  `65` tinyint(4) NOT NULL,
  `66` tinyint(4) NOT NULL,
  `67` tinyint(4) NOT NULL,
  `68` tinyint(4) NOT NULL,
  `69` tinyint(4) NOT NULL,
  `70` tinyint(4) NOT NULL,
  `71` tinyint(4) NOT NULL,
  `72` tinyint(4) NOT NULL,
  `73` tinyint(4) NOT NULL,
  `74` tinyint(4) NOT NULL,
  `75` tinyint(4) NOT NULL,
  `76` tinyint(4) NOT NULL,
  `77` tinyint(4) NOT NULL,
  `78` tinyint(4) NOT NULL,
  `79` tinyint(4) NOT NULL,
  `80` tinyint(4) NOT NULL,
  `81` tinyint(4) NOT NULL,
  `82` tinyint(4) NOT NULL,
  `83` tinyint(4) NOT NULL,
  `84` tinyint(4) NOT NULL,
  `85` tinyint(4) NOT NULL,
  `86` tinyint(4) NOT NULL,
  `87` tinyint(4) NOT NULL,
  `88` tinyint(4) NOT NULL,
  `89` tinyint(4) NOT NULL,
  `90` tinyint(4) NOT NULL,
  `91` tinyint(4) NOT NULL,
  `92` tinyint(4) NOT NULL,
  `93` tinyint(4) NOT NULL,
  `94` tinyint(4) NOT NULL,
  `95` tinyint(4) NOT NULL,
  `96` tinyint(4) NOT NULL,
  `97` tinyint(4) NOT NULL,
  `98` tinyint(4) NOT NULL,
  `99` tinyint(4) NOT NULL,
  `100` tinyint(4) NOT NULL,
  `101` tinyint(4) NOT NULL,
  `102` tinyint(4) NOT NULL,
  `103` tinyint(4) NOT NULL,
  `104` tinyint(4) NOT NULL,
  `105` tinyint(4) NOT NULL,
  `106` tinyint(4) NOT NULL,
  `107` tinyint(4) NOT NULL,
  `108` tinyint(4) NOT NULL,
  `109` tinyint(4) NOT NULL,
  `110` tinyint(4) NOT NULL,
  `111` tinyint(4) NOT NULL,
  `112` tinyint(4) NOT NULL,
  `113` tinyint(4) NOT NULL,
  `114` tinyint(4) NOT NULL,
  `115` tinyint(4) NOT NULL,
  `116` tinyint(4) NOT NULL,
  `117` tinyint(4) NOT NULL,
  `118` tinyint(4) NOT NULL,
  `119` tinyint(4) NOT NULL,
  `120` tinyint(4) NOT NULL,
  `121` tinyint(4) NOT NULL,
  `122` tinyint(4) NOT NULL,
  `123` tinyint(4) NOT NULL,
  `124` tinyint(4) NOT NULL,
  `125` tinyint(4) NOT NULL,
  `126` tinyint(4) NOT NULL,
  `127` tinyint(4) NOT NULL,
  `128` tinyint(4) NOT NULL,
  `129` tinyint(4) NOT NULL,
  `130` tinyint(4) NOT NULL,
  `131` tinyint(4) NOT NULL,
  `132` tinyint(4) NOT NULL,
  `133` tinyint(4) NOT NULL,
  `134` tinyint(4) NOT NULL,
  `135` tinyint(4) NOT NULL,
  `136` tinyint(4) NOT NULL,
  `137` tinyint(4) NOT NULL,
  `138` tinyint(4) NOT NULL,
  `139` tinyint(4) NOT NULL,
  `140` tinyint(4) NOT NULL,
  `141` tinyint(4) NOT NULL,
  `142` tinyint(4) NOT NULL,
  `143` tinyint(4) NOT NULL,
  `144` tinyint(4) NOT NULL,
  `145` tinyint(4) NOT NULL,
  `146` tinyint(4) NOT NULL,
  `147` tinyint(4) NOT NULL,
  `148` tinyint(4) NOT NULL,
  `149` tinyint(4) NOT NULL,
  `150` tinyint(4) NOT NULL,
  `151` tinyint(4) NOT NULL,
  `152` tinyint(4) NOT NULL,
  `153` tinyint(4) NOT NULL,
  `154` tinyint(4) NOT NULL,
  `155` tinyint(4) NOT NULL,
  `156` tinyint(4) NOT NULL,
  `157` tinyint(4) NOT NULL,
  `158` tinyint(4) NOT NULL,
  `159` tinyint(4) NOT NULL,
  `160` tinyint(4) NOT NULL,
  `161` tinyint(4) NOT NULL,
  `162` tinyint(4) NOT NULL,
  `163` tinyint(4) NOT NULL,
  `164` tinyint(4) NOT NULL,
  `165` tinyint(4) NOT NULL,
  `166` tinyint(4) NOT NULL,
  `167` tinyint(4) NOT NULL,
  `168` tinyint(4) NOT NULL,
  `169` tinyint(4) NOT NULL,
  `170` tinyint(4) NOT NULL,
  `171` tinyint(4) NOT NULL,
  `172` tinyint(4) NOT NULL,
  `173` tinyint(4) NOT NULL,
  `174` tinyint(4) NOT NULL,
  `175` tinyint(4) NOT NULL,
  `176` tinyint(4) NOT NULL,
  `177` tinyint(4) NOT NULL,
  `178` tinyint(4) NOT NULL,
  `179` tinyint(4) NOT NULL,
  `180` tinyint(4) NOT NULL,
  `181` tinyint(4) NOT NULL,
  `182` tinyint(4) NOT NULL,
  `183` tinyint(4) NOT NULL,
  `184` tinyint(4) NOT NULL,
  `185` tinyint(4) NOT NULL,
  `186` tinyint(4) NOT NULL,
  `187` tinyint(4) NOT NULL,
  `188` tinyint(4) NOT NULL,
  `189` tinyint(4) NOT NULL,
  `190` tinyint(4) NOT NULL,
  `191` tinyint(4) NOT NULL,
  `192` tinyint(4) NOT NULL,
  `193` tinyint(4) NOT NULL,
  `194` tinyint(4) NOT NULL,
  `195` tinyint(4) NOT NULL,
  `196` tinyint(4) NOT NULL,
  `197` tinyint(4) NOT NULL,
  `198` tinyint(4) NOT NULL,
  `199` tinyint(4) NOT NULL,
  `200` tinyint(4) NOT NULL,
  `201` tinyint(4) NOT NULL,
  `202` tinyint(4) NOT NULL,
  `203` tinyint(4) NOT NULL,
  `204` tinyint(4) NOT NULL,
  `205` tinyint(4) NOT NULL,
  `206` tinyint(4) NOT NULL,
  `207` tinyint(4) NOT NULL,
  `208` tinyint(4) NOT NULL,
  `209` tinyint(4) NOT NULL,
  `210` tinyint(4) NOT NULL,
  `211` tinyint(4) NOT NULL,
  `212` tinyint(4) NOT NULL,
  `213` tinyint(4) NOT NULL,
  `214` tinyint(4) NOT NULL,
  `215` tinyint(4) NOT NULL,
  `216` tinyint(4) NOT NULL,
  `217` tinyint(4) NOT NULL,
  `218` tinyint(4) NOT NULL,
  `219` tinyint(4) NOT NULL,
  `220` tinyint(4) NOT NULL,
  `221` tinyint(4) NOT NULL,
  `222` tinyint(4) NOT NULL,
  `223` tinyint(4) NOT NULL,
  `224` tinyint(4) NOT NULL,
  `225` tinyint(4) NOT NULL,
  `226` tinyint(4) NOT NULL,
  `227` tinyint(4) NOT NULL,
  `228` tinyint(4) NOT NULL,
  `229` tinyint(4) NOT NULL,
  `230` tinyint(4) NOT NULL,
  `231` tinyint(4) NOT NULL,
  `232` tinyint(4) NOT NULL,
  `233` tinyint(4) NOT NULL,
  `234` tinyint(4) NOT NULL,
  `235` tinyint(4) NOT NULL,
  `236` tinyint(4) NOT NULL,
  `237` tinyint(4) NOT NULL,
  `238` tinyint(4) NOT NULL,
  `239` tinyint(4) NOT NULL,
  `240` tinyint(4) NOT NULL,
  `241` tinyint(4) NOT NULL,
  `242` tinyint(4) NOT NULL,
  `243` tinyint(4) NOT NULL,
  `244` tinyint(4) NOT NULL,
  `245` tinyint(4) NOT NULL,
  `246` tinyint(4) NOT NULL,
  `247` tinyint(4) NOT NULL,
  `248` tinyint(4) NOT NULL,
  `249` tinyint(4) NOT NULL,
  `250` tinyint(4) NOT NULL,
  `251` tinyint(4) NOT NULL,
  `252` tinyint(4) NOT NULL,
  `253` tinyint(4) NOT NULL,
  `254` tinyint(4) NOT NULL,
  `255` tinyint(4) NOT NULL,
  `256` tinyint(4) NOT NULL,
  `257` tinyint(4) NOT NULL,
  `258` tinyint(4) NOT NULL,
  `259` tinyint(4) NOT NULL,
  `260` tinyint(4) NOT NULL,
  `261` tinyint(4) NOT NULL,
  `262` tinyint(4) NOT NULL,
  `263` tinyint(4) NOT NULL,
  `264` tinyint(4) NOT NULL,
  `265` tinyint(4) NOT NULL,
  `266` tinyint(4) NOT NULL,
  `267` tinyint(4) NOT NULL,
  `268` tinyint(4) NOT NULL,
  `269` tinyint(4) NOT NULL,
  `270` tinyint(4) NOT NULL,
  `271` tinyint(4) NOT NULL,
  `272` tinyint(4) NOT NULL,
  `273` tinyint(4) NOT NULL,
  `274` tinyint(4) NOT NULL,
  `275` tinyint(4) NOT NULL,
  `276` tinyint(4) NOT NULL,
  `277` tinyint(4) NOT NULL,
  `278` tinyint(4) NOT NULL,
  `279` tinyint(4) NOT NULL,
  `280` tinyint(4) NOT NULL,
  `281` tinyint(4) NOT NULL,
  `282` tinyint(4) NOT NULL,
  `283` tinyint(4) NOT NULL,
  `284` tinyint(4) NOT NULL,
  `285` tinyint(4) NOT NULL,
  `286` tinyint(4) NOT NULL,
  `287` tinyint(4) NOT NULL,
  `288` tinyint(4) NOT NULL,
  `289` tinyint(4) NOT NULL,
  `290` tinyint(4) NOT NULL,
  `291` tinyint(4) NOT NULL,
  `292` tinyint(4) NOT NULL,
  `293` tinyint(4) NOT NULL,
  `294` tinyint(4) NOT NULL,
  `295` tinyint(4) NOT NULL,
  `296` tinyint(4) NOT NULL,
  `297` tinyint(4) NOT NULL,
  `298` tinyint(4) NOT NULL,
  `299` tinyint(4) NOT NULL,
  `300` tinyint(4) NOT NULL,
  `301` tinyint(4) NOT NULL,
  `302` tinyint(4) NOT NULL,
  `303` tinyint(4) NOT NULL,
  `304` tinyint(4) NOT NULL,
  `305` tinyint(4) NOT NULL,
  `306` tinyint(4) NOT NULL,
  `307` tinyint(4) NOT NULL,
  `308` tinyint(4) NOT NULL,
  `309` tinyint(4) NOT NULL,
  `310` tinyint(4) NOT NULL,
  `311` tinyint(4) NOT NULL,
  `312` tinyint(4) NOT NULL,
  `313` tinyint(4) NOT NULL,
  `314` tinyint(4) NOT NULL,
  `315` tinyint(4) NOT NULL,
  `316` tinyint(4) NOT NULL,
  `317` tinyint(4) NOT NULL,
  `318` tinyint(4) NOT NULL,
  `319` tinyint(4) NOT NULL,
  `320` tinyint(4) NOT NULL,
  `321` tinyint(4) NOT NULL,
  `322` tinyint(4) NOT NULL,
  `323` tinyint(4) NOT NULL,
  `324` tinyint(4) NOT NULL,
  `325` tinyint(4) NOT NULL,
  `326` tinyint(4) NOT NULL,
  `327` tinyint(4) NOT NULL,
  `328` tinyint(4) NOT NULL,
  `329` tinyint(4) NOT NULL,
  `330` tinyint(4) NOT NULL,
  `331` tinyint(4) NOT NULL,
  `332` tinyint(4) NOT NULL,
  `333` tinyint(4) NOT NULL,
  `334` tinyint(4) NOT NULL,
  `335` tinyint(4) NOT NULL,
  `336` tinyint(4) NOT NULL,
  `337` tinyint(4) NOT NULL,
  `338` tinyint(4) NOT NULL,
  `339` tinyint(4) NOT NULL,
  `340` tinyint(4) NOT NULL,
  `341` tinyint(4) NOT NULL,
  `342` tinyint(4) NOT NULL,
  `343` tinyint(4) NOT NULL,
  `344` tinyint(4) NOT NULL,
  `345` tinyint(4) NOT NULL,
  `346` tinyint(4) NOT NULL,
  `347` tinyint(4) NOT NULL,
  `348` tinyint(4) NOT NULL,
  `349` tinyint(4) NOT NULL,
  `350` tinyint(4) NOT NULL,
  `351` tinyint(4) NOT NULL,
  `352` tinyint(4) NOT NULL,
  `353` tinyint(4) NOT NULL,
  `354` tinyint(4) NOT NULL,
  `355` tinyint(4) NOT NULL,
  `356` tinyint(4) NOT NULL,
  `357` tinyint(4) NOT NULL,
  `358` tinyint(4) NOT NULL,
  `359` tinyint(4) NOT NULL,
  `360` tinyint(4) NOT NULL,
  `361` tinyint(4) NOT NULL,
  `362` tinyint(4) NOT NULL,
  `363` tinyint(4) NOT NULL,
  `364` tinyint(4) NOT NULL,
  `365` tinyint(4) NOT NULL,
  `366` tinyint(4) NOT NULL,
  `367` tinyint(4) NOT NULL,
  `368` tinyint(4) NOT NULL,
  `369` tinyint(4) NOT NULL,
  `370` tinyint(4) NOT NULL,
  `371` tinyint(4) NOT NULL,
  `372` tinyint(4) NOT NULL,
  `373` tinyint(4) NOT NULL,
  `374` tinyint(4) NOT NULL,
  `375` tinyint(4) NOT NULL,
  `376` tinyint(4) NOT NULL,
  `377` tinyint(4) NOT NULL,
  `378` tinyint(4) NOT NULL,
  `379` tinyint(4) NOT NULL,
  `380` tinyint(4) NOT NULL,
  `381` tinyint(4) NOT NULL,
  `382` tinyint(4) NOT NULL,
  `383` tinyint(4) NOT NULL,
  `384` tinyint(4) NOT NULL,
  `385` tinyint(4) NOT NULL,
  `386` tinyint(4) NOT NULL,
  `387` tinyint(4) NOT NULL,
  `388` tinyint(4) NOT NULL,
  `389` tinyint(4) NOT NULL,
  `390` tinyint(4) NOT NULL,
  `391` tinyint(4) NOT NULL,
  `392` tinyint(4) NOT NULL,
  `393` tinyint(4) NOT NULL,
  `394` tinyint(4) NOT NULL,
  `395` tinyint(4) NOT NULL,
  `396` tinyint(4) NOT NULL,
  `397` tinyint(4) NOT NULL,
  `398` tinyint(4) NOT NULL,
  `399` tinyint(4) NOT NULL,
  `400` tinyint(4) NOT NULL,
  `401` tinyint(4) NOT NULL,
  `402` tinyint(4) NOT NULL,
  `403` tinyint(4) NOT NULL,
  `404` tinyint(4) NOT NULL,
  `405` tinyint(4) NOT NULL,
  `406` tinyint(4) NOT NULL,
  `407` tinyint(4) NOT NULL,
  `408` tinyint(4) NOT NULL,
  `409` tinyint(4) NOT NULL,
  `410` tinyint(4) NOT NULL,
  `411` tinyint(4) NOT NULL,
  `412` tinyint(4) NOT NULL,
  `413` tinyint(4) NOT NULL,
  `414` tinyint(4) NOT NULL,
  `415` tinyint(4) NOT NULL,
  `416` tinyint(4) NOT NULL,
  `417` tinyint(4) NOT NULL,
  `418` tinyint(4) NOT NULL,
  `419` tinyint(4) NOT NULL,
  `420` tinyint(4) NOT NULL,
  `421` tinyint(4) NOT NULL,
  `422` tinyint(4) NOT NULL,
  `423` tinyint(4) NOT NULL,
  `424` tinyint(4) NOT NULL,
  `425` tinyint(4) NOT NULL,
  `426` tinyint(4) NOT NULL,
  `427` tinyint(4) NOT NULL,
  `428` tinyint(4) NOT NULL,
  `429` tinyint(4) NOT NULL,
  `430` tinyint(4) NOT NULL,
  `431` tinyint(4) NOT NULL,
  `432` tinyint(4) NOT NULL,
  `433` tinyint(4) NOT NULL,
  `434` tinyint(4) NOT NULL,
  `435` tinyint(4) NOT NULL,
  `436` tinyint(4) NOT NULL,
  `437` tinyint(4) NOT NULL,
  `438` tinyint(4) NOT NULL,
  `439` tinyint(4) NOT NULL,
  `440` tinyint(4) NOT NULL,
  `441` tinyint(4) NOT NULL,
  `442` tinyint(4) NOT NULL,
  `443` tinyint(4) NOT NULL,
  `444` tinyint(4) NOT NULL,
  `445` tinyint(4) NOT NULL,
  `446` tinyint(4) NOT NULL,
  `447` tinyint(4) NOT NULL,
  `448` tinyint(4) NOT NULL,
  `449` tinyint(4) NOT NULL,
  `450` tinyint(4) NOT NULL,
  `451` tinyint(4) NOT NULL,
  `452` tinyint(4) NOT NULL,
  `453` tinyint(4) NOT NULL,
  `454` tinyint(4) NOT NULL,
  `455` tinyint(4) NOT NULL,
  `456` tinyint(4) NOT NULL,
  `457` tinyint(4) NOT NULL,
  `458` tinyint(4) NOT NULL,
  `459` tinyint(4) NOT NULL,
  `460` tinyint(4) NOT NULL,
  `461` tinyint(4) NOT NULL,
  `462` tinyint(4) NOT NULL,
  `463` tinyint(4) NOT NULL,
  `464` tinyint(4) NOT NULL,
  `465` tinyint(4) NOT NULL,
  `466` tinyint(4) NOT NULL,
  `467` tinyint(4) NOT NULL,
  `468` tinyint(4) NOT NULL,
  `469` tinyint(4) NOT NULL,
  `470` tinyint(4) NOT NULL,
  `471` tinyint(4) NOT NULL,
  `472` tinyint(4) NOT NULL,
  `473` tinyint(4) NOT NULL,
  `474` tinyint(4) NOT NULL,
  `475` tinyint(4) NOT NULL,
  `476` tinyint(4) NOT NULL,
  `477` tinyint(4) NOT NULL,
  `478` tinyint(4) NOT NULL,
  `479` tinyint(4) NOT NULL,
  `480` tinyint(4) NOT NULL,
  `481` tinyint(4) NOT NULL,
  `482` tinyint(4) NOT NULL,
  `483` tinyint(4) NOT NULL,
  `484` tinyint(4) NOT NULL,
  `485` tinyint(4) NOT NULL,
  `486` tinyint(4) NOT NULL,
  `487` tinyint(4) NOT NULL,
  `488` tinyint(4) NOT NULL,
  `489` tinyint(4) NOT NULL,
  `490` tinyint(4) NOT NULL,
  `491` tinyint(4) NOT NULL,
  `492` tinyint(4) NOT NULL,
  `493` tinyint(4) NOT NULL,
  PRIMARY KEY (`pokedexId`),
  KEY `player_dex_idx` (`memberId`),
  CONSTRAINT `Player_dex_fk` FOREIGN KEY (`memberId`) REFERENCES `pn_members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_pokedex
-- ----------------------------
INSERT INTO `pn_pokedex` VALUES ('6', '6', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('7', '7', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('8', '8', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('9', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('10', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('11', '11', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '2', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('12', '12', '0', '0', '0', '0', '0', '1', '1', '0', '1', '1', '1', '0', '1', '1', '0', '1', '0', '0', '1', '2', '1', '0', '1', '0', '1', '1', '1', '0', '1', '0', '0', '2', '0', '0', '1', '1', '2', '0', '1', '0', '1', '0', '1', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '1', '1', '0', '2', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '2', '1', '2', '0', '1', '1', '1', '1', '0', '1', '0', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '1', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '1', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('13', '13', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '1', '0', '0', '1', '1', '0', '1', '0', '1', '1', '0', '0', '0', '1', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('14', '14', '1', '1', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '2', '2', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '2', '2', '1', '0', '0', '2', '2', '0', '2', '1', '0', '1', '1', '1', '1', '1', '2', '2', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '1', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '1', '0', '0', '1', '2', '1', '0', '0', '0', '1', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0', '1', '1', '1', '0', '1', '0', '1', '0', '0', '1', '0', '0', '1', '1', '1', '0', '0', '1', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('15', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('16', '16', '1', '1', '0', '0', '0', '1', '0', '0', '1', '1', '0', '0', '1', '0', '0', '1', '0', '0', '1', '1', '1', '0', '1', '1', '0', '1', '1', '1', '1', '0', '1', '1', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '2', '2', '1', '1', '1', '1', '0', '1', '1', '0', '1', '0', '0', '1', '1', '1', '1', '2', '1', '1', '1', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '0', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '1', '1', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0', '1', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('17', '17', '2', '2', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '1', '0', '0', '1', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '2', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('18', '18', '0', '0', '0', '2', '2', '0', '0', '0', '0', '1', '1', '0', '1', '1', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('19', '19', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pn_pokedex` VALUES ('20', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '1', '1', '0', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', '0', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `pn_pokemon`
-- ----------------------------
DROP TABLE IF EXISTS `pn_pokemon`;
CREATE TABLE `pn_pokemon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) DEFAULT NULL,
  `speciesName` varchar(32) DEFAULT NULL,
  `exp` float(9,2) DEFAULT NULL,
  `baseExp` tinyint(3) unsigned DEFAULT NULL,
  `expType` varchar(16) DEFAULT NULL,
  `isFainted` varchar(5) DEFAULT NULL,
  `level` tinyint(3) unsigned DEFAULT NULL,
  `happiness` tinyint(3) unsigned DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `nature` varchar(24) DEFAULT NULL,
  `abilityName` varchar(24) DEFAULT NULL,
  `itemName` varchar(28) DEFAULT NULL,
  `isShiny` varchar(5) DEFAULT NULL,
  `originalTrainerName` varchar(12) DEFAULT NULL,
  `currentTrainerName` varchar(12) DEFAULT NULL,
  `contestStats` varchar(25) DEFAULT NULL,
  `move0` varchar(32) DEFAULT NULL,
  `move1` varchar(32) DEFAULT NULL,
  `move2` varchar(32) DEFAULT NULL,
  `move3` varchar(32) DEFAULT NULL,
  `hp` smallint(6) DEFAULT NULL,
  `atk` smallint(6) DEFAULT NULL,
  `def` smallint(6) DEFAULT NULL,
  `speed` smallint(6) DEFAULT NULL,
  `spATK` smallint(6) DEFAULT NULL,
  `spDEF` smallint(6) DEFAULT NULL,
  `evHP` tinyint(3) unsigned DEFAULT NULL,
  `evATK` tinyint(3) unsigned DEFAULT NULL,
  `evDEF` tinyint(3) unsigned DEFAULT NULL,
  `evSPD` tinyint(3) unsigned DEFAULT NULL,
  `evSPATK` tinyint(3) unsigned DEFAULT NULL,
  `evSPDEF` tinyint(3) unsigned DEFAULT NULL,
  `ivHP` tinyint(4) DEFAULT NULL,
  `ivATK` tinyint(4) DEFAULT NULL,
  `ivDEF` tinyint(4) DEFAULT NULL,
  `ivSPD` tinyint(4) DEFAULT NULL,
  `ivSPATK` tinyint(4) DEFAULT NULL,
  `ivSPDEF` tinyint(4) DEFAULT NULL,
  `pp0` tinyint(4) DEFAULT NULL,
  `pp1` tinyint(4) DEFAULT NULL,
  `pp2` tinyint(4) DEFAULT NULL,
  `pp3` tinyint(4) DEFAULT NULL,
  `maxpp0` tinyint(4) DEFAULT NULL,
  `maxpp1` tinyint(4) DEFAULT NULL,
  `maxpp2` tinyint(4) DEFAULT NULL,
  `maxpp3` tinyint(4) DEFAULT NULL,
  `ppUp0` tinyint(3) unsigned DEFAULT NULL,
  `ppUp1` tinyint(3) unsigned DEFAULT NULL,
  `ppUp2` tinyint(3) unsigned DEFAULT NULL,
  `ppUp3` tinyint(3) unsigned DEFAULT NULL,
  `date` varchar(25) DEFAULT NULL,
  `caughtWith` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pn_pokemon
-- ----------------------------
INSERT INTO `pn_pokemon` VALUES ('14', 'Totodile', 'Totodile', '135.00', '66', 'PARABOLIC', 'false', '5', '255', '1', 'Quirky', 'Torrent', '', 'false', 'Rabbit', 'Rabbit', '0,0,0,0,0', 'Scratch', 'Leer', 'null', 'null', '21', '11', '11', '10', '9', '10', '0', '0', '0', '0', '0', '0', '23', '3', '0', '31', '6', '5', '35', '30', '-1', '-1', '35', '30', '-1', '-1', '0', '0', '0', '0', '2014-09-17:21-43-31', null);
INSERT INTO `pn_pokemon` VALUES ('15', 'Totodile', 'Totodile', '135.00', '66', 'PARABOLIC', 'false', '5', '140', '2', 'Quirky', 'Torrent', '', 'false', 'conejoo', 'conejoo', '0,0,0,0,0', 'Scratch', 'Leer', 'null', 'null', '21', '12', '12', '9', '9', '9', '0', '0', '0', '0', '0', '0', '27', '28', '31', '8', '3', '2', '35', '30', '-1', '-1', '35', '30', '-1', '-1', '0', '0', '0', '0', '2014-09-18:02-57-55', null);
INSERT INTO `pn_pokemon` VALUES ('16', 'Totodile', 'Totodile', '280.00', '66', 'PARABOLIC', 'false', '7', '255', '1', 'Quirky', 'Torrent', '', 'false', 'snupy', 'snupy', '0,0,0,0,0', 'Scratch', 'Leer', 'Water Gun', 'null', '24', '15', '15', '12', '11', '13', '0', '0', '0', '1', '0', '0', '0', '16', '19', '19', '10', '31', '35', '30', '25', '-1', '35', '30', '25', '-1', '0', '0', '0', '0', '2014-09-18:02-59-13', null);
INSERT INTO `pn_pokemon` VALUES ('17', 'Torchic', 'Torchic', '267.00', '65', 'PARABOLIC', 'false', '7', '153', '1', 'Quirky', 'Blaze', '', 'false', 'tumacho', 'tumacho', '0,0,0,0,0', 'Scratch', 'Growl', 'Focus Energy', 'null', '24', '14', '10', '12', '16', '13', '0', '0', '0', '2', '0', '0', '13', '13', '3', '15', '18', '16', '35', '40', '30', '-1', '35', '40', '30', '-1', '0', '0', '0', '0', '2014-09-18:03-30-01', null);
INSERT INTO `pn_pokemon` VALUES ('18', 'Mudkip', 'Mudkip', '1559.00', '65', 'PARABOLIC', 'false', '13', '255', '1', 'Quirky', 'Torrent', '', 'false', 'probando', 'probando', '0,0,0,0,0', 'Tackle', 'Growl', 'Mud-Slap', 'Water Gun', '39', '25', '20', '16', '18', '20', '3', '1', '8', '13', '0', '0', '29', '17', '16', '6', '0', '18', '35', '40', '10', '25', '35', '40', '10', '25', '0', '0', '0', '0', '2014-09-19:22-20-35', null);
INSERT INTO `pn_pokemon` VALUES ('19', 'Weedle', 'Weedle', '64.00', '52', 'MEDIUM', 'false', '4', '255', '1', 'Quirky', 'Shield Dust', '', 'false', 'probando', 'probando', '0,0,0,0,0', 'Poison Sting', 'String Shot', 'null', 'null', '17', '8', '7', '9', '7', '6', '0', '0', '0', '0', '0', '0', '10', '7', '1', '15', '16', '6', '35', '40', '-1', '-1', '35', '40', '-1', '-1', '0', '0', '0', '0', '2014-09-21:01-19-29', '35');
INSERT INTO `pn_pokemon` VALUES ('20', 'Pikachu', 'Pikachu', '64.00', '82', 'MEDIUM', 'false', '4', '255', '1', 'Jolly', 'Static', '', 'false', 'probando', 'probando', '0,0,0,0,0', 'Thundershock', 'Growl', 'null', 'null', '17', '10', '8', '14', '9', '8', '0', '0', '0', '0', '0', '0', '8', '19', '30', '21', '31', '14', '30', '40', '-1', '-1', '30', '40', '-1', '-1', '0', '0', '0', '0', '2014-09-21:05-48-49', '35');
INSERT INTO `pn_pokemon` VALUES ('21', 'Swampert', 'Swampert', '61755.00', '65', 'PARABOLIC', 'false', '41', '255', '1', 'Quirky', 'Torrent', 'Oran Berry', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Brick Break', 'Mud Shot', 'Mud Bomb', 'Muddy Water', '143', '121', '110', '58', '79', '82', '87', '168', '255', '0', '0', '0', '4', '22', '13', '10', '11', '8', '15', '15', '10', '10', '15', '15', '10', '10', '0', '0', '0', '0', '2014-09-21:08-56-48', null);
INSERT INTO `pn_pokemon` VALUES ('22', 'Nidoran-m', 'Nidoran-m', '58.00', '60', 'PARABOLIC', 'false', '3', '255', '1', 'Docile', 'Poison Point', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Leer', 'Peck', 'null', 'null', '16', '9', '7', '8', '8', '7', '0', '0', '0', '0', '0', '0', '27', '28', '3', '3', '29', '10', '30', '35', '-1', '-1', '30', '35', '-1', '-1', '0', '0', '0', '0', '2014-09-21:12-35-58', '35');
INSERT INTO `pn_pokemon` VALUES ('23', 'Croconaw', 'Croconaw', '13804.00', '66', 'PARABOLIC', 'false', '26', '153', '1', 'Quirky', 'Torrent', '', 'false', 'mapplebox', 'mapplebox', '0,0,0,0,0', 'Scratch', 'Bite', 'Water Gun', 'Ice Fang', '76', '53', '46', '41', '39', '38', '12', '21', '6', '63', '2', '0', '21', '22', '0', '10', '16', '2', '35', '25', '25', '15', '35', '25', '25', '15', '0', '0', '0', '0', '2014-09-21:13-07-00', null);
INSERT INTO `pn_pokemon` VALUES ('24', 'Gastly', 'Gastly', '802.00', '95', 'PARABOLIC', 'false', '11', '153', '1', 'Bashful', 'Levitate', '', 'false', 'mapplebox', 'mapplebox', '0,0,0,0,0', 'Hypnosis', 'Lick', 'Mean Look', 'Spite', '28', '15', '13', '23', '27', '14', '0', '2', '0', '6', '1', '0', '9', '23', '14', '9', '2', '18', '20', '20', '5', '10', '20', '20', '5', '10', '0', '0', '0', '0', '2014-09-21:13-24-48', '35');
INSERT INTO `pn_pokemon` VALUES ('25', 'Swampert', 'Swampert', '365237.00', '65', 'PARABOLIC', 'false', '71', '255', '1', 'Quirky', 'Torrent', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Hammer Arm', 'Mud Shot', 'Protect', 'Water Gun', '234', '191', '153', '128', '143', '147', '14', '48', '59', '105', '3', '1', '12', '31', '15', '28', '24', '21', '10', '15', '10', '25', '10', '15', '10', '25', '0', '0', '0', '0', '2014-09-21:14-25-23', null);
INSERT INTO `pn_pokemon` VALUES ('26', 'Treecko', 'Treecko', '201.00', '65', 'PARABOLIC', 'false', '6', '163', '1', 'Quirky', 'Overgrow', '', 'false', 'Wobble', 'Wobble', '0,0,0,0,0', 'Pound', 'Leer', 'Absorb', 'null', '19', '10', '9', '12', '12', '11', '0', '2', '0', '0', '0', '0', '18', '28', '11', '2', '29', '29', '32', '30', '20', '-1', '35', '30', '20', '-1', '0', '0', '0', '0', '2014-09-21:15-49-26', null);
INSERT INTO `pn_pokemon` VALUES ('27', 'Infernape', 'Infernape', '125872.00', '65', 'PARABOLIC', 'false', '51', '255', '1', 'Quirky', 'Blaze', '', 'false', 'Noah', 'Noah', '0,0,0,0,0', 'Flame Wheel', 'Close Combat', 'Flamethrower', 'Taunt', '140', '118', '79', '141', '117', '83', '0', '10', '0', '128', '0', '0', '3', '13', '4', '19', '13', '11', '25', '5', '15', '20', '25', '5', '15', '20', '0', '0', '0', '0', '2014-09-21:16-00-17', null);
INSERT INTO `pn_pokemon` VALUES ('28', 'Graveler', 'Graveler', '75712.00', '86', 'PARABOLIC', 'false', '43', '255', '2', 'Careful', 'Rock Head', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Mud Sport', 'Defense Curl', 'Tackle', 'Selfdestruct', '110', '97', '105', '44', '43', '52', '3', '6', '3', '22', '0', '0', '22', '23', '4', '17', '12', '9', '15', '40', '35', '5', '15', '40', '35', '5', '0', '0', '0', '0', '2014-09-21:15-57-33', '35');
INSERT INTO `pn_pokemon` VALUES ('29', 'Primeape', 'Primeape', '61890.00', '74', 'MEDIUM', 'false', '39', '255', '2', 'Lonely', 'Vital Spirit', '', 'false', 'Noah', 'Noah', '0,0,0,0,0', 'Fury Swipes', 'Low Kick', 'Assurance', 'Brick Break', '108', '96', '58', '91', '62', '70', '20', '3', '16', '25', '0', '0', '18', '2', '30', '25', '28', '28', '15', '20', '10', '15', '15', '20', '10', '15', '0', '0', '0', '0', '2014-09-21:16-06-57', '35');
INSERT INTO `pn_pokemon` VALUES ('30', 'Ivysaur', 'Ivysaur', '14701.00', '64', 'PARABOLIC', 'false', '26', '88', '1', 'Quirky', 'Overgrow', 'Oran Berry', 'false', 'esmeralda', 'esmeralda', '0,0,0,0,0', 'Tackle', 'Leech Seed', 'Razor Leaf', 'Vine Whip', '71', '41', '43', '42', '47', '47', '6', '6', '9', '49', '0', '0', '15', '13', '21', '12', '4', '5', '35', '10', '25', '10', '35', '10', '25', '10', '0', '0', '0', '0', '2014-09-21:16-09-32', null);
INSERT INTO `pn_pokemon` VALUES ('31', 'Snubbull', 'Snubbull', '800.00', '63', 'FAST', 'false', '10', '153', '2', 'Adamant', 'Intimidate', '', 'false', 'mapplebox', 'mapplebox', '0,0,0,0,0', 'Fire Fang', 'Bite', 'Ice Fang', 'Tackle', '35', '23', '16', '14', '13', '13', '0', '0', '0', '0', '0', '0', '31', '7', '10', '30', '18', '5', '15', '25', '15', '35', '15', '25', '15', '35', '0', '0', '0', '0', '2014-09-21:16-07-54', '35');
INSERT INTO `pn_pokemon` VALUES ('32', 'Arcanine', 'Arcanine', '468134.00', '91', 'SLOW', 'false', '72', '255', '2', 'Naughty', 'Intimidate', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Bite', 'Flamethrower', 'Reversal', 'Flame Wheel', '218', '195', '143', '169', '161', '115', '1', '61', '64', '29', '3', '0', '9', '5', '16', '31', '16', '12', '25', '15', '15', '25', '25', '15', '15', '25', '0', '0', '0', '0', '2014-09-21:16-09-15', '35');
INSERT INTO `pn_pokemon` VALUES ('33', 'Kadabra', 'Kadabra', '232637.00', '73', 'PARABOLIC', 'false', '61', '255', '1', 'Rash', 'Inner Focus', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Teleport', 'Psycho Cut', 'Reflect', 'Psybeam', '122', '58', '57', '141', '181', '94', '0', '33', '32', '27', '2', '13', '5', '9', '18', '7', '22', '20', '20', '20', '20', '20', '20', '20', '20', '20', '0', '0', '0', '0', '2014-09-21:19-28-25', '36');
INSERT INTO `pn_pokemon` VALUES ('34', 'Geodude', 'Geodude', '560.00', '86', 'PARABOLIC', 'false', '10', '255', '1', 'Lonely', 'Rock Head', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Defense Curl', 'Mud Sport', 'Tackle', 'Rock Polish', '29', '26', '23', '11', '12', '11', '0', '0', '0', '0', '0', '0', '18', '29', '9', '26', '19', '4', '40', '15', '35', '30', '40', '15', '35', '30', '0', '0', '0', '0', '2014-09-22:00-00-37', '35');
INSERT INTO `pn_pokemon` VALUES ('35', 'Charmeleon', 'Charmeleon', '3039.00', '65', 'PARABOLIC', 'false', '16', '255', '1', 'Quirky', 'Blaze', '', 'false', 'fernamaton', 'fernamaton', '0,0,0,0,0', 'Scratch', 'Growl', 'Ember', 'Smokescreen', '50', '28', '24', '32', '34', '28', '23', '9', '0', '22', '0', '0', '30', '19', '5', '8', '22', '15', '35', '40', '22', '20', '35', '40', '25', '20', '0', '0', '0', '0', '2014-09-22:01-20-11', null);
INSERT INTO `pn_pokemon` VALUES ('36', 'Squirtle', 'Squirtle', '201.00', '66', 'PARABOLIC', 'false', '6', '153', '1', 'Quirky', 'Torrent', '', 'false', 'Yokil', 'Yokil', '0,0,0,0,0', 'Tackle', 'null', 'Tail Whip', 'null', '21', '11', '13', '10', '11', '14', '1', '0', '0', '1', '0', '0', '4', '14', '11', '4', '11', '30', '35', '-1', '30', '-1', '35', '-1', '30', '-1', '0', '0', '0', '0', '2014-09-22:01-56-44', null);
INSERT INTO `pn_pokemon` VALUES ('37', 'Nidorino', 'Nidorino', '55746.00', '60', 'PARABOLIC', 'false', '39', '255', '1', 'Brave', 'Poison Point', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Toxic Spikes', 'Peck', 'Fury Attack', 'Double Kick', '109', '81', '53', '53', '58', '57', '24', '18', '4', '6', '0', '0', '26', '28', '9', '8', '26', '24', '20', '35', '20', '30', '20', '35', '20', '30', '0', '0', '0', '0', '2014-09-22:05-59-46', '36');
INSERT INTO `pn_pokemon` VALUES ('38', 'Vulpix', 'Vulpix', '2744.00', '63', 'MEDIUM', 'false', '14', '255', '2', 'Hasty', 'Flash Fire', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Ember', 'Tail Whip', 'Quick Attack', 'Roar', '3', '16', '15', '26', '19', '27', '0', '0', '0', '0', '0', '0', '8', '2', '11', '6', '0', '31', '24', '28', '29', '20', '25', '30', '30', '20', '0', '0', '0', '0', '2014-09-22:13-54-13', '35');
INSERT INTO `pn_pokemon` VALUES ('39', 'Tentacool', 'Tentacool', '33750.00', '105', 'SLOW', 'false', '30', '255', '2', 'Jolly', 'Clear Body', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Water Pulse', 'Toxic Spikes', 'Bubblebeam', 'Supersonic', '65', '35', '27', '52', '33', '68', '0', '0', '0', '0', '0', '0', '6', '22', '4', '3', '6', '12', '20', '20', '20', '30', '20', '20', '20', '30', '0', '0', '0', '0', '2014-09-22:14-23-19', '35');
INSERT INTO `pn_pokemon` VALUES ('40', 'Raticate', 'Raticate', '50653.00', '116', 'MEDIUM', 'false', '37', '255', '1', 'Mild', 'Run Away', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Focus Energy', 'Pursuit', 'Sucker Punch', 'Bite', '88', '69', '50', '85', '58', '61', '0', '0', '0', '0', '0', '0', '2', '12', '18', '23', '29', '14', '30', '20', '5', '25', '30', '20', '5', '25', '0', '0', '0', '0', '2014-09-22:14-24-45', '35');
INSERT INTO `pn_pokemon` VALUES ('41', 'Growlithe', 'Growlithe', '49130.00', '91', 'SLOW', 'false', '34', '255', '2', 'Bashful', 'Flash Fire', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Roar', 'Fire Fang', 'Helping Hand', 'Ember', '31', '54', '42', '53', '55', '47', '0', '0', '0', '0', '0', '0', '22', '6', '20', '22', '8', '25', '19', '14', '18', '25', '20', '15', '20', '25', '0', '0', '0', '0', '2014-09-22:19-12-11', '35');
INSERT INTO `pn_pokemon` VALUES ('42', 'Growlithe', 'Growlithe', '49130.00', '91', 'SLOW', 'false', '34', '255', '2', 'Rash', 'Intimidate', '', 'false', 'Teepo', 'Teepo', '0,0,0,0,0', 'Helping Hand', 'Fire Fang', 'Take Down', 'Leer', '90', '63', '40', '52', '65', '36', '0', '0', '0', '0', '0', '0', '26', '31', '13', '21', '21', '5', '20', '15', '20', '30', '20', '15', '20', '30', '0', '0', '0', '0', '2014-09-22:19-11-28', '35');
INSERT INTO `pn_pokemon` VALUES ('43', 'Mankey', 'Mankey', '1169.00', '74', 'MEDIUM', 'false', '10', '255', '2', 'Adamant', 'Vital Spirit', '', 'false', 'fernamaton', 'fernamaton', '0,0,0,0,0', 'Scratch', 'Low Kick', 'Leer', 'Fury Swipes', '23', '23', '13', '18', '10', '13', '0', '0', '12', '0', '0', '0', '15', '26', '19', '15', '4', '6', '27', '19', '29', '15', '35', '20', '30', '15', '0', '0', '0', '0', '2014-09-23:03-25-08', '36');
INSERT INTO `pn_pokemon` VALUES ('44', 'Mankey', 'Mankey', '125.00', '74', 'MEDIUM', 'false', '5', '255', '1', 'Bold', 'Vital Spirit', '', 'false', 'fernamaton', 'fernamaton', '0,0,0,0,0', 'Scratch', 'Low Kick', 'Leer', 'Focus Energy', '19', '13', '10', '12', '9', '10', '0', '0', '0', '0', '0', '0', '4', '31', '24', '0', '13', '19', '35', '20', '30', '30', '35', '20', '30', '30', '0', '0', '0', '0', '2014-09-23:03-22-08', '36');
INSERT INTO `pn_pokemon` VALUES ('46', 'Noctowl', 'Noctowl', '19683.00', '162', 'MEDIUM', 'false', '27', '255', '2', 'Hasty', 'Insomnia', '', 'false', 'Noah', 'Noah', '0,0,0,0,0', 'Growl', 'Foresight', 'Tackle', 'Confusion', '93', '39', '34', '50', '48', '57', '0', '0', '0', '0', '0', '0', '8', '27', '22', '10', '8', '2', '40', '40', '35', '25', '40', '40', '35', '25', '0', '0', '0', '0', '2014-10-01:10-59-37', '35');
INSERT INTO `pn_pokemon` VALUES ('47', 'Tentacruel', 'Tentacruel', '40960.00', '205', 'SLOW', 'false', '32', '255', '1', 'Hasty', 'Liquid Ooze', '', 'false', 'Noah', 'Noah', '0,0,0,0,0', 'Barrier', 'Wrap', 'Bubblebeam', 'Toxic Spikes', '98', '51', '47', '84', '58', '82', '0', '0', '0', '0', '0', '0', '16', '5', '21', '24', '6', '1', '30', '20', '20', '20', '30', '20', '20', '20', '0', '0', '0', '0', '2014-10-01:15-23-28', '36');
INSERT INTO `pn_pokemon` VALUES ('48', 'Electabuzz', 'Electabuzz', '64161.00', '156', 'MEDIUM', 'false', '40', '255', '1', 'Impish', 'Static', '', 'false', 'Matz115', 'Matz115', '0,0,0,0,0', 'Swift', 'Thunderpunch', 'Discharge', 'Quick Attack', '104', '81', '59', '97', '81', '76', '0', '0', '0', '11', '0', '0', '5', '24', '8', '19', '25', '9', '20', '15', '15', '30', '20', '15', '15', '30', '0', '0', '0', '0', '2014-10-02:19-55-28', '36');
INSERT INTO `pn_pokemon` VALUES ('49', 'Sandslash', 'Sandslash', '1000.00', '163', 'MEDIUM', 'false', '10', '88', '1', 'Relaxed', 'Sand Veil', 'Oran Berry', 'false', 'esmeralda', 'esmeralda', '0,0,0,0,0', 'Scratch', 'Defense Curl', 'Sand-Attack', 'Poison Sting', '36', '27', '30', '18', '15', '17', '0', '0', '0', '0', '0', '0', '17', '26', '10', '20', '15', '12', '35', '40', '15', '35', '35', '40', '15', '35', '0', '0', '0', '0', '2014-10-06:03-28-23', '35');
INSERT INTO `pn_pokemon` VALUES ('50', 'Granbull', 'Granbull', '1758.00', '178', 'FAST', 'false', '13', '88', '2', 'Relaxed', 'Intimidate', '', 'false', 'esmeralda', 'esmeralda', '0,0,0,0,0', 'Ice Fang', 'Tackle', 'Fire Fang', 'Lick', '48', '39', '31', '16', '20', '21', '0', '0', '0', '0', '0', '0', '18', '25', '30', '12', '0', '6', '15', '35', '15', '20', '15', '35', '15', '20', '0', '0', '0', '0', '2014-10-06:03-57-14', '36');
INSERT INTO `pn_pokemon` VALUES ('51', 'Quilava', 'Quilava', '5712.00', '65', 'PARABOLIC', 'false', '20', '255', '2', 'Quirky', 'Blaze', '', 'false', 'Zorro', 'Zorro', '0,0,0,0,0', 'Tackle', 'Quick Attack', 'Ember', 'Smokescreen', '29', '35', '20', '37', '41', '36', '17', '15', '8', '28', '2', '9', '17', '28', '24', '3', '31', '31', '35', '2', '3', '19', '35', '30', '25', '20', '0', '0', '0', '0', '2014-10-08:05-23-04', null);
