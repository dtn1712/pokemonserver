package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.feature.battle.BattleTurn;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class PokemonSwitchEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player p = session.getPlayer();
        int pIndex = request.readInt();
        BattleTurn turn;
        if (p.isBattling())
            if (p.getParty()[pIndex] != null)
                if (!p.getParty()[pIndex].isFainted()) {
                    turn = BattleTurn.getSwitchTurn(pIndex);
                    try {
                        p.getBattleField().queueMove(p.getBattleId(), turn);
                    } catch (Exception e) {
                    } // this is dubious and check it
                }
    }

}
