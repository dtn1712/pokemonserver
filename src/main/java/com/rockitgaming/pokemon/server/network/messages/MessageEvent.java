package com.rockitgaming.pokemon.server.network.messages;

import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public interface MessageEvent {
    void parse(Session session, ClientMessage request, ServerMessage message);
}