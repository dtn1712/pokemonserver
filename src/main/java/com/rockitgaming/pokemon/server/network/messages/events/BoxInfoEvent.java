package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class BoxInfoEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {

        Player p = session.getPlayer();
        if (p.isBoxing()) {
            int boxNum = request.readInt();
            if (boxNum >= 0 && boxNum < 9)
                p.sendBoxInfo(boxNum);
        }
    }
}
