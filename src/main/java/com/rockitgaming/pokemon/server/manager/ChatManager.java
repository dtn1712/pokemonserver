package com.rockitgaming.pokemon.server.manager;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.models.entity.Player.Language;
import com.rockitgaming.pokemon.server.models.map.ServerMap;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;
import org.apache.mina.core.session.IoSession;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Handles chat messages sent by players
 */
public class ChatManager implements Runnable {
    
    /* Local chat queue
     * [Message, x, y] */
    private Queue<Object[]> localQueue;

    /* Private chat queue
     * [session, sender, message] */
    private Queue<Object[]> privateQueue;

    private Thread thread;
    private boolean isRunning = false;

    /**
     * Default Constructor
     */
    public ChatManager() {
        thread = new Thread(this, "ChatManager-Thread");
        localQueue = new ConcurrentLinkedQueue<>();
        privateQueue = new ConcurrentLinkedQueue<>();
    }

    /**
     * Returns how many messages are queued in this chat manager
     *
     * @return
     */
    public int getProcessingLoad() {
        return localQueue.size() + privateQueue.size();
    }

    /**
     * Queues a local chat message
     *
     * @param message
     * @param mapX
     * @param mapY
     */
    public void queueLocalChatMessage(String message, int mapX, int mapY, Language l) {
        localQueue.add(new Object[]{message, String.valueOf(mapX), String.valueOf(mapY), String.valueOf(l)});
    }

    /**
     * Queues a private chat message
     *
     * @param message
     * @param receiver
     * @param sender
     */
    public void queuePrivateMessage(String message, IoSession receiver, String sender) {
        privateQueue.add(new Object[]{receiver, sender, message});
    }

    /**
     * Called by thread.start()
     */
    public void run() {
        Object[] o;
        ServerMap m;
        Session s;
        while (isRunning) {
            /* Send next local chat message. */
            if (localQueue.peek() != null) {
                o = localQueue.poll();
                m = GameServer.getServiceManager().getMovementService().getMapMatrix().getMapByGamePosition(Integer.parseInt((String) o[1]), Integer.parseInt((String) o[2]));
                if (m != null)
                    m.sendChatMessage((String) o[0], Language.valueOf((String) o[3]));
            }
			/* Send next private chat message. */
            if (privateQueue.peek() != null) {
                o = privateQueue.poll();
                s = (Session) o[0];
                if (s.getLoggedIn()) {
                    ServerMessage startBattle = new ServerMessage(s);
                    startBattle.init(ClientPacket.CHAT_PACKET.getValue());
                    startBattle.addString("p" + o[2]);
                    startBattle.sendResponse();
                }
            }
            try {
                Thread.sleep(250);
            } catch (Exception e) {
            }
        }
    }

    /**
     * Start this chat manager
     */
    public void start() {
        isRunning = true;
        thread.start();
    }

    /**
     * Stop this chat manager
     */
    public void stop() {
        isRunning = false;
    }
}