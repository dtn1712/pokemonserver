package com.rockitgaming.pokemon.server.network.protocol;

import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.constants.ClientPacket;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.buffer.ChannelBuffers;

import java.io.IOException;
import java.nio.charset.Charset;

public class ServerMessage {
    private ChannelBuffer body;
    private ChannelBufferOutputStream bodyStream;
    private String message;
    private Session player;

    public ServerMessage(ClientPacket id) {
        init(id.getValue());
        message = "";
    }

    public ServerMessage(Session session) {
        player = session;
        message = "";
    }

    public void addBool(Boolean obj) {
        try {
            bodyStream.writeBoolean(obj);
            message = message + ";BOOL: " + obj;
        } catch (IOException e) {
        }
    }

    public void addByte(int obj) {
        try {
            bodyStream.writeByte(obj);
            message = message + ";BYTE: " + obj;
        } catch (IOException e) {
        }
    }

    public void addInt(Integer obj) {
        try {
            bodyStream.writeInt(obj);
            message = message + ";INT: " + obj;
        } catch (IOException e) {
        }
    }

    public void addShort(int obj) {
        try {
            bodyStream.writeShort((short) obj);
            message = message + ";SHORT: " + obj;
        } catch (IOException e) {
        }
    }

    public void addString(String obj) {
        try {
            if (obj == null) {
                // sometimes this string is null which is causing some issues (battle freezing for example)
                bodyStream.writeShort(0);
                bodyStream.writeChars("");
                message = message + ";STRING: " + "";
            } else {
                bodyStream.writeShort(obj.length());
                bodyStream.writeChars(obj);
                message = message + ";STRING: " + obj;
            }
        } catch (IOException e) {
            System.out.println("There was an error adding a String");
            e.printStackTrace();
        }
    }

    public ChannelBuffer get() {
        return body;
    }

    public String getBodyString() {
        String str = new String(body.toString(Charset.defaultCharset()));
        String consoleText = str;
        for (int i = 0; i < 13; i++)
            consoleText = consoleText.replace(Character.toString((char) i), "{" + i + "}");
        return consoleText;
    }

    public String getMessage() {
        return message;
    }

    public void init(int id) {
        body = ChannelBuffers.dynamicBuffer();
        bodyStream = new ChannelBufferOutputStream(body);
        message = "[Out] -> ID" + id;
        body.writeByte(id);
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendResponse() {
        player.Send(this);
    }
}
