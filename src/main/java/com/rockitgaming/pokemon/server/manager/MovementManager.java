package com.rockitgaming.pokemon.server.manager;

import com.rockitgaming.pokemon.server.models.entity.Character;
import com.rockitgaming.pokemon.server.models.entity.HMObject;
import com.rockitgaming.pokemon.server.models.entity.HMObject.ObjectType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * Loops through all players and moves them if they request to be moved
 */
public class MovementManager implements Runnable {

    private final ReentrantReadWriteLock queueLock = new ReentrantReadWriteLock(true);
    private final ReadLock qReadLock = queueLock.readLock();
    private final WriteLock qWriteLock = queueLock.writeLock();

    private Comparator<Character> comp;
    private boolean isRunning = true;
    private Queue<Character> done;
    private int pLoad = 0;
    private Queue<Character> waiting;
    private Queue<Character> moving;

    /**
     * Default constructor.
     */
    public MovementManager() {
        comp = (arg0, arg1) -> arg0.getPriority() - arg1.getPriority();
        waiting = new PriorityQueue<>(11, comp);
        done = new PriorityQueue<>(10, comp);
        moving = new PriorityQueue<>(1, comp);
    }

    public void addHMObject(HMObject obj) {
        qWriteLock.lock();
        try {
            if (obj.getType() == ObjectType.STRENGTH_BOULDER) {
                pLoad++;
                waiting.add(obj);
            }
        } finally {
            qWriteLock.unlock();
        }
    }

    /**
     * Adds a player to this movement service
     *
     * @param player
     */
    public void addPlayer(Character player) {
        qWriteLock.lock();
        try {
            pLoad++;
            waiting.offer(player);
        } finally {
            qWriteLock.unlock();
        }
    }

    /**
     * Returns how many players are in this thread (the processing load)
     */
    public int getProcessingLoad() {
        return pLoad;
    }

    /**
     * Returns true if the movement manager is running
     */
    public boolean isRunning() {
        return Thread.currentThread().isAlive();
    }

    /**
     * Removes a player from this movement service, returns true if the player
     * was in the thread and was removed. Otherwise, returns false.
     *
     * @param player
     */
    public boolean removePlayer(String player) {
        /* Check waiting list */
        synchronized (waiting) {
            for (Character c : waiting)
                if (c.getName().equalsIgnoreCase(player)) {
                    waiting.remove(c);
                    pLoad--;
                    return true;
                }
        }
		/* Check done list */
        synchronized (done) {
            for (Character c : done)
                if (c.getName().equalsIgnoreCase(player)) {
                    done.remove(c);
                    pLoad--;
                    return true;
                }
        }
		/* Check moving list */
        synchronized (moving) {
            for (Character c : moving)
                if (c.getName().equalsIgnoreCase(player)) {
                    moving.remove(c);
                    pLoad--;
                    return true;
                }
        }
        return false;
    }

    /**
     * Called by thread.start(). Looping through the waiting and moving queues
     * and moving the character with highest priority from both queues.
     */
    public void run() {
        Character tmp = null;
        while (isRunning) {
			/* Pull char of highest priority */
            if (waiting != null && !waiting.isEmpty()) {
                synchronized (waiting) {
                    tmp = waiting.poll();
                }
				/* Move character */
                if (tmp.move()) {
					/* Place him in moving queue */
                    qReadLock.lock();
                    try {
                        moving.offer(tmp);
                    } finally {
                        qReadLock.unlock();
                    }
                }
                if (!moving.isEmpty())
                    synchronized (moving) {
						/* Get character */
                        tmp = moving.poll();
                    }
				/* Move character */
                if (!tmp.move()) {
					/* Place him in the done queue */
                    qReadLock.lock();
                    try {
                        done.offer(tmp);
                    } finally {
                        qReadLock.unlock();
                    }
                } else {
					/* Keep him in the Moving queue, but place him last */
                    qReadLock.lock();
                    try {
                        moving.offer(tmp);
                    } finally {
                        qReadLock.unlock();
                    }
                }

            }
            if (waiting != null && done != null) {
                if (waiting.isEmpty()) {
					/*
					 * If waiting is empty transfer the characters with movement
					 * remaining
					 */
                    ArrayList<Character> transfer = new ArrayList<Character>();
                    for (int i = 0; i < done.size(); i++) {
                        if (done.peek().peekNextMovement() != null)
							/*
							 * Character has movement remaining. Adding to the
							 * transfer list
							 */
                            transfer.add(done.poll());
                        else
							/*
							 * Character has no movement remaining. Removing
							 * from movement service
							 */
                            done.poll();
                    }
                    qReadLock.lock();
                    try {
                        waiting.addAll(transfer);
						/*
						 * the done queue should be clear, this is just to be
						 * sure!
						 */
                        done.clear();
                    } finally {
                        qReadLock.unlock();
                    }
                }
            }
            try {
                Thread.sleep(3);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    /**
     * Stops the movement thread
     */
    public void stop() {
        moving.clear();
        done.clear();
        waiting.clear();
        isRunning = false;
    }
}