/**
 * 
 */
package com.rockitgaming.pokemon.server.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class Utility {

	private static final Log log = LogFactory.getLog(Utility.class);

	/**
	 * Load 1 file text
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String loadTextFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }
	
	/**
	 * load file config java
	 * @param path
	 */
	public static Properties loadConfigFile(String path) {
        FileInputStream inputStream = null;
        Properties returnProperties = null;
        try {
            returnProperties = new Properties();
            inputStream = new FileInputStream(path);
            returnProperties.load(inputStream);
            inputStream.close();
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return returnProperties;
    }

}
