package com.rockitgaming.pokemon.server;

import com.rockitgaming.pokemon.server.manager.DatabaseManager;
import com.rockitgaming.pokemon.server.services.ServiceManager;
import com.rockitgaming.pokemon.server.util.Utility;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

public class GameServer {

    private static final Log log = LogFactory.getLog(GameServer.class);

    private static GameServer instance;
    private static ServiceManager serviceManager;
    private static DatabaseManager databaseManager;


    public static GameServer getInstance() {
        if (instance == null) {
            instance = new GameServer();
        }
        return instance;
    }

    private GameServer() {
        ServerConfig.init(Utility.loadConfigFile(Constants.CONFIG_FOLDER_PATH + "serverconfig.properties"));
    }

    public static ServiceManager getServiceManager() {
        return serviceManager;
    }

    public void start() throws ServerException {
        databaseManager = DatabaseManager.getInstance();
        serviceManager = new ServiceManager();
        serviceManager.start();
    }

    public void stop() {
        try {
            serviceManager.stop();
            Thread.sleep(Constants.SLEEP_TIME);
            log.info("Exiting server...");
            databaseManager.close();
            System.exit(0);
        } catch (InterruptedException e) {
            log.error("Failed during stop server",e);
        }
    }

    public static void main(String[] args) throws Exception {
        PropertyConfigurator.configure(Constants.CONFIG_FOLDER_PATH + "log4j.properties");
        GameServer.getInstance().start();
    }
}
