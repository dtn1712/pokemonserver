package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.GameServer;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class ChangePasswordEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        String[] details = request.readString().split(",");
        GameServer.getServiceManager().getGameRunnerService().getLoginManager().queuePasswordChange(session, details[0], details[1], details[2]);
    }

}
