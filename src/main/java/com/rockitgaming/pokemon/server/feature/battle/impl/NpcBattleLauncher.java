package com.rockitgaming.pokemon.server.feature.battle.impl;

import com.rockitgaming.pokemon.server.models.entity.NPC;
import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.models.entity.Positionable.Direction;
import com.rockitgaming.pokemon.server.services.DataService;

/**
 * When the player is challenged by an NPC, this class moves the player to the
 * NPC and launches a battle
 */
public class NpcBattleLauncher implements Runnable {

    private NPC npc;
    private Player player;

    public NpcBattleLauncher(NPC n, Player p) {
        npc = n;
        /* Ensure the NPC cannot battle anyone else */
        npc.updateLastBattleTime();
        player = p;
    }

    /**
     * Moves the player to the npc and starts the battle
     */
    public void run() {
        try {
			/*
			 * Set that the player is battling so we can control their movement.
			 * A movement request from the client could mess things up.
			 */
            player.setBattling(true);
            npc.challengePlayer(player);
			/* Sleep for a moment */
            Thread.sleep(1000);
			/* Make the player face the npc */
            switch (npc.getFacing()) {
                case Up:
                    player.setFacing(Direction.Down);
                    break;
                case Down:
                    player.setFacing(Direction.Up);
                    break;
                case Left:
                    player.setFacing(Direction.Right);
                    break;
                case Right:
                    player.setFacing(Direction.Left);
                    break;
            }
			/* Start the NPC battle */
            player.ensureHealthyPokemon();
            player.setBattleField(new NpcBattleField(DataService.getBattleMechanics(), player, npc));
        } catch (Exception e) {
            player.setBattling(false);
            e.printStackTrace();
        }
    }
}
