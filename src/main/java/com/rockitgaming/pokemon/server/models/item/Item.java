package com.rockitgaming.pokemon.server.models.item;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;

/**
 * Represents an item
 */
public class Item {

    /* Handles item attributes, lets us know what we need to do with them */
    public enum ItemAttribute {
        BATTLE, CRAFT, FIELD, HOLD, MOVESLOT, OTHER, POKEMON
    }

    @ElementList
    private ArrayList<ItemAttribute> attributes;
    @Element
    private String category;
    @Element
    private String description;
    @Element
    private int id;
    @Element
    private String name;
    @Element
    private int price;

    @Element
    private int shop;

    public void addAttribute(ItemAttribute attribute) {
        if (attributes == null)
            attributes = new ArrayList<>();
        attributes.add(attribute);
    }

    public ArrayList<ItemAttribute> getAttributes() {
        return attributes;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getShop() {
        return shop;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setShop(int shop) {
        this.shop = shop;
    }
}
