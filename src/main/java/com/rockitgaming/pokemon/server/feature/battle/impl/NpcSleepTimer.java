package com.rockitgaming.pokemon.server.feature.battle.impl;

import com.rockitgaming.pokemon.server.models.entity.NPC;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * A thread which wakes sleeping NPCs (NPCs sleep for ~15 minutes after battle)
 */
public class NpcSleepTimer implements Runnable {

    private static final Log log = LogFactory.getLog(NpcSleepTimer.class);

    private static List<NPC> npcSleeping = new ArrayList<>();
    private boolean running = true;


    public static void addNPC(NPC npc) {
        npcSleeping.add(npc);
    }

    @Override
    public void run() {
        log.info("Npc sleep timer started");
        Random r = new Random();
        while (running) {
            /* Loop through every sleeping NPC. */
            Iterator<NPC> iterator = npcSleeping.iterator();
            while (iterator.hasNext()) {
                NPC npc = iterator.next();
                if (!npc.canBattle() && System.currentTimeMillis() - npc.getLastBattleTime() >= 5 * 60 * 1000
                        + r.nextInt(10 * 60 * 1000)) {
                    npc.resetLastBattleTime();
                    iterator.remove();
                }
            }
            try {
                Thread.sleep(r.nextInt(5 * 60 * 1000)); // ~5 minutes
            } catch (InterruptedException e) {
            }
        }
        log.info("Npc sleep timer stopped");
    }

    public void stop() {
        running = false;
    }
}
