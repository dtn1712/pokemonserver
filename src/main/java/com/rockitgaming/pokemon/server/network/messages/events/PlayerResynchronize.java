package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class PlayerResynchronize implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        // Get Player
        Player p = session.getPlayer();

        // Resync
        p.sendToBeResynchronized();
    }
}