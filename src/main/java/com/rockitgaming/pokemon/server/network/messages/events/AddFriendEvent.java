package com.rockitgaming.pokemon.server.network.messages.events;

import com.rockitgaming.pokemon.server.models.entity.Player;
import com.rockitgaming.pokemon.server.network.connections.Session;
import com.rockitgaming.pokemon.server.network.connections.ActiveConnections;
import com.rockitgaming.pokemon.server.network.messages.MessageEvent;
import com.rockitgaming.pokemon.server.network.protocol.ClientMessage;
import com.rockitgaming.pokemon.server.network.protocol.ServerMessage;

public class AddFriendEvent implements MessageEvent {

    public void parse(Session session, ClientMessage request, ServerMessage message) {
        Player p = session.getPlayer();
        String friend = request.readString();
        if (ActiveConnections.getPlayer(friend) != null)
            p.addFriend(friend);
    }
}
