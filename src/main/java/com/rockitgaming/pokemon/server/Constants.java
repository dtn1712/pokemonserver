package com.rockitgaming.pokemon.server;

/**
 * Created by dangn on 7/10/16.
 */
public class Constants {

    public static final String CONFIG_FOLDER_PATH = "src/main/config/";

    public static final int SLEEP_TIME = 10 * 1000;

    public static final int MOVEMENT_THREADS = 12;
    public static final int REVISION = 1;
    public static final double RATE_GOLD = 1.0;
    public static final double RATE_EXP_POKE = 150.0;
    public static final double RATE_EXP_TRAINER = 1.0;

    public static final String DATABASE_CONFIG_FILE_NAME = Constants.CONFIG_FOLDER_PATH + "dbconfig.properties";
}
